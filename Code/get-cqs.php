<?php

//http://localhost/get-cqs.php?filter_global_id=008659&filter_hierarchy_level=0&hierarchy_name=organizational&start_date=2016-12-01&end_date=2016-12-31&grouping_hierarchy_level=0&modality=IHD,PD&grouping_metric_level=2&filter_metric_id=0

$filter_global_id = !isset($_GET["filter_global_id"]) ? '' : urldecode($_GET["filter_global_id"]); 								//facility_id (padded with zeros to make 6 characters), or global_id of area, region, group and division
$filter_hierarchy_level = !isset($_GET["filter_hierarchy_level"]) ? '' : urldecode($_GET["filter_hierarchy_level"]); 			// 0=facility, 1=area, 2=region, 3=group, 4=division'
$hierarchy_name = !isset($_GET["hierarchy_name"]) ? '' : urldecode($_GET["hierarchy_name"]); 									// organizational, ESCO'
$start_date = !isset($_GET["start_date"]) ? '' : urldecode($_GET["start_date"]); 												// YYYY-MM-DD format'
$end_date = !isset($_GET["end_date"]) ? '' : urldecode($_GET["end_date"]); 														// YYYY-MM-DD format'
$grouping_hierarchy_level = !isset($_GET["grouping_hierarchy_level"]) ? '' : urldecode($_GET["grouping_hierarchy_level"]); 		// 0=facility, 1=area, 2=region, 3=group, 4=division'
$modality = !isset($_GET["modality"]) ? '' : urldecode($_GET["modality"]); 														// IHD, HHD, PD'
$grouping_metric_level = !isset($_GET["grouping_metric_level"]) ? '' : urldecode($_GET["grouping_metric_level"]); 				// 1=Total, 2=category, 3=group, 4=metric/modality'
$filter_metric_id = !isset($_GET["filter_metric_id"]) ? '' : urldecode($_GET["filter_metric_id"]); 								// metric id
$scenario = !isset($_GET["scenario"]) ? '' : urldecode($_GET["scenario"]); 														// scenario in json format


$filter_global_id = '\''.str_replace(',', '\',\'', $filter_global_id).'\'';
$modality = '\''.str_replace(',', '\'\',\'\'', $modality).'\'';

$db = '//hax-exa-scan:1521/KCNGXPS';

$conn = oci_connect('BI_SRC', 'welcome', $db) or die('connection failed');

if (!$conn) {
     $e = oci_error();
     trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
	echo('error');
}

if ($filter_metric_id > 0) {
	$whereclause = 'where metric_id = '.$filter_metric_id;
}
else {
	$whereclause = '';
}

//$scenario == ''

if ($filter_metric_id > 0) {
	$sql = 'select t.*, PERCENT_RANK() OVER (ORDER BY metric_value) as rank
			from (TABLE(get_metricDataObjects(
				'.$filter_global_id.', 
				'.$filter_hierarchy_level.', 
				\''.$hierarchy_name.'\', 
				\''.$start_date.'\', 
				\''.$end_date.'\', 
				'.$grouping_hierarchy_level.', 
				\'\''.$modality.'\'\', 
				'.$grouping_metric_level.',
				\'\'
				))) t
			'.$whereclause.'
			';
			
	$stmt = oci_parse($conn, $sql);
	oci_execute($stmt) or die($sql.'<br><br>query failed');

	$result = array();

	$result['rows'] = array();

		while ($r = oci_fetch_array($stmt, OCI_BOTH)) {
				$result['rows'][] = $r;
		}

	oci_free_statement($stmt);	
	$result['sql'] = $sql;	
}

else {
	$boxplot_sql = '
		select metric_id, metric_label, 

		case when min(standardized_score) >
				2.5 * PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY standardized_score ASC) - 1.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score ASC)
			 then min(standardized_score)
			 else
				2.5 * PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY standardized_score ASC) - 1.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score ASC)
			 end min_b,   
		PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY standardized_score ASC) percent25_b,
		MEDIAN(standardized_score) median_b, 
		PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score ASC) percent75_b,

		case when max(standardized_score) >
				2.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score ASC) - 1.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score ASC)
			 then max(standardized_score)
			 else
				2.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score ASC) - 1.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score ASC)
			 end max_b,   

		case when min(standardized_score_scenario) >
				2.5 * PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY standardized_score_scenario ASC) - 1.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score_scenario ASC)
			 then min(standardized_score)
			 else
				2.5 * PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY standardized_score_scenario ASC) - 1.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score_scenario ASC)
			 end min_s,   
		PERCENTILE_CONT(0.25) WITHIN GROUP (ORDER BY standardized_score_scenario ASC) percent25_s,
		MEDIAN(standardized_score_scenario) median_s, 
		PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score_scenario ASC) percent75_s,
		case when max(standardized_score_scenario) >
				2.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score_scenario ASC) - 1.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score_scenario ASC)
			 then max(standardized_score)
			 else
				2.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score_scenario ASC) - 1.5 * PERCENTILE_CONT(0.75) WITHIN GROUP (ORDER BY standardized_score_scenario ASC)
			 end max_s,
		count(hierarchy_id) as clinic_count
		from (
			select b.*, s.standardized_score standardized_score_scenario
			from (TABLE(get_metricDataObjects(
				'.$filter_global_id.', 
				'.$filter_hierarchy_level.', 
				\''.$hierarchy_name.'\', 
				\''.$start_date.'\', 
				\''.$end_date.'\', 
				'.$grouping_hierarchy_level.', 
				\'\''.$modality.'\'\', 
				4,
				\''.$scenario.'\'
				))) s,
				(TABLE(get_metricDataObjects(
				'.$filter_global_id.', 
				'.$filter_hierarchy_level.', 
				\''.$hierarchy_name.'\', 
				\''.$start_date.'\', 
				\''.$end_date.'\', 
				'.$grouping_hierarchy_level.', 
				\'\''.$modality.'\'\', 
				4,
				\'\'
				))) b
			where s.metric_id = b.metric_id
			and s.hierarchy_id = b.hierarchy_id
			and s.post_date = b.post_date
			and b.cqs_flag = 1
		) bs_detail
		group by metric_id, metric_label			
			';

	$stmt = oci_parse($conn, $boxplot_sql);
	oci_execute($stmt) or die($boxplot_sql.'<br><br>query failed');

	$result = array();

	$result['boxplot'] = array();

		while ($r = oci_fetch_array($stmt, OCI_BOTH)) {
				$result['boxplot'][] = $r;
		}

	oci_free_statement($stmt);						
			
	$sql = 'select cast(((nvl(CQS_SCENARIO,0) - nvl(CQS_BASELINE,0)) * 4) as INTEGER)/4 as CQS_CHANGE, count(HIERARCHY_ID) as CQS_FREQUENCY
			from (
				select s.HIERARCHY_ID, s.HIERARCHY_LABEL, s.METRIC_LABEL, s.CQS_SCORE as CQS_SCENARIO, b.CQS_SCORE as CQS_BASELINE
				from (TABLE(get_metricDataObjects(
					'.$filter_global_id.', 
					'.$filter_hierarchy_level.', 
					\''.$hierarchy_name.'\', 
					\''.$start_date.'\', 
					\''.$end_date.'\', 
					'.$grouping_hierarchy_level.', 
					\'\''.$modality.'\'\', 
					'.$grouping_metric_level.',
					\''.$scenario.'\'
					))) s,
					(TABLE(get_metricDataObjects(
					'.$filter_global_id.', 
					'.$filter_hierarchy_level.', 
					\''.$hierarchy_name.'\', 
					\''.$start_date.'\', 
					\''.$end_date.'\', 
					'.$grouping_hierarchy_level.', 
					\'\''.$modality.'\'\', 
					'.$grouping_metric_level.',
					\'\'
					))) b
				where s.metric_id = b.metric_id
				and s.hierarchy_id = b.hierarchy_id
				and s.post_date = b.post_date
				and b.cqs_flag = 1
			) scenario_detail
			group by cast(((nvl(CQS_SCENARIO,0) - nvl(CQS_BASELINE,0)) * 4) as INTEGER)/4
			';	

	$stmt = oci_parse($conn, $sql);
	oci_execute($stmt) or die($sql.'<br><br>query failed');

	$result['rows'] = array();

		while ($r = oci_fetch_array($stmt, OCI_BOTH)) {
				$result['rows'][] = $r;
		}

	oci_free_statement($stmt);
	$result['sql'] = $sql;
	$result['boxplot_sql'] = $boxplot_sql;

	}



// get thresholds
$sql = 'select metric_id, metric_label, metric_goal_low, metric_goal_med, METRIC_GOAL_HIGH, CQS_FLAG
		from metric_data_dim
		where metric_id = '.$filter_metric_id.'
		';
$stmt = oci_parse($conn, $sql);
oci_execute($stmt) or die($sql.'<br><br>query failed');
		
$result['thresholds'] = array();

	while ($r = oci_fetch_array($stmt, OCI_BOTH)) {
			$result['thresholds'][] = $r;
	}

oci_free_statement($stmt);
oci_close($conn);

//header('Content-Type: application/json');

echo json_encode($result, JSON_PRETTY_PRINT);

?>
