<?php

$model = !isset($_GET["model"]) ? '' : urldecode($_GET["model"]);
$mrn = !isset($_GET["mrn"]) ? '' : urldecode($_GET["mrn"]);
$startdate = !isset($_GET["startdate"]) ? '' : urldecode($_GET["startdate"]);
$enddate = !isset($_GET["enddate"]) ? '' : urldecode($_GET["enddate"]);

$mrn = '\''.str_replace(',', '\',\'', $mrn).'\'';
$model = '\''.str_replace(',', '\',\'', $model).'\'';

$db = '//hax-exa-scan:1521/KCNGXPS';

$conn = oci_connect('BI_SRC', 'welcome', $db) or die('connection failed');

if (!$conn) {
     $e = oci_error();
     trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
	echo('error');
}

//$sql = 'select subject_json
if ($startdate == '') {
	$sql = 'select rtrim(subject_json, \'}\') || \',"insert_date":"\' || to_char(insert_date, \'YYYY-MM-DD\') || \'"}\' as subject_json
			from PRED_MODEL_FACT pmf
			where SUBJECT_ID in ('.$mrn.')
			and source_id in (select source_id from PRED_MODEL_SOURCE_DIM where source_nm in ('.$model.'))
			and insert_date >= (select max(insert_date) as maxinsertdate from PRED_MODEL_FACT where source_id = pmf.source_id) - 1/24
			';
}
else {	
	$sql = 'select rtrim(subject_json, \'}\') || \',"insert_date":"\' || to_char(insert_date, \'YYYY-MM-DD\') || \'"}\' as subject_json
			from PRED_MODEL_FACT 
			where SUBJECT_ID in ('.$mrn.')
			and source_id in (select source_id from PRED_MODEL_SOURCE_DIM where source_nm in ('.$model.'))
			and insert_date between to_date(\''.$startdate.'\', \'YYYY-MM-DD\') and to_date(\''.$enddate.'\', \'YYYY-MM-DD\')
			';
}

$stmt = oci_parse($conn, $sql);
oci_execute($stmt) or die('query failed'.$sql);

$result = array();

$result['model'] = $model;
$result['mrn'] = $mrn;
$result['startdate'] = $startdate;
$result['enddate'] = $enddate;
$result['rows'] = array();

	while ($r = oci_fetch_array($stmt, OCI_BOTH)) {
			$result['rows'][] = json_decode($r[0]);
	}
		
oci_free_statement($stmt);
oci_close($conn);

header('Content-Type: application/json');

// add MODEL NAME and DATES to JSON output

echo json_encode($result, JSON_PRETTY_PRINT);

?>
