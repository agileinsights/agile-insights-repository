<!DOCTYPE html>
<html lang="en">
<head>
  <title>agile insights</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <style>
    .row.content {height: 1500px}
  </style>
</head>

<nav class="navbar navbar-inverse navbar-fixed-top" style="margin-bottom: 0px;">
  <div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
    </div>
	<div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">

	  </ul>
    </div>
  </div>
</nav>
<body>

<div class="container-fluid">
	<div class="row content">
	    <div class="col-sm-3 sidenav" id="nav">
			<h2>File Upload</h2>
			<br>	
			<form id="upload" name="upload" onsubmit="return false">
			    Select file to upload:
				<input id="files" type="file" name="files" value="" />
				<input hidden id="uploadsubmit" type="submit" name="uploadsubmit" value="Upload" onclick="return uploadfile();"/>
			</form>
			<br>
			<output id="list"></output>
			<br>
			<button class="btn btn-default" style="display:none;" alt="upload" title='upload' id="uploadbutton">Upload</button>
		</div>
		<div class="col-sm-9 contentview">
			<div id='title'></div>
			<div id="topmenu" style="display:inline-block; float:right;">
			</div>
			<div id="sampledata" style="block"></div>
		</div>
	</div>
</div>

<div id="loadingModal" class="modal fade">
	<center><img src="../images/loading_icon.gif"></center>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="https://raw.githubusercontent.com/mholt/PapaParse/master/papaparse.min.js"></script>

<script>

var filedata;

var uploadform = document.forms['upload'];
var formSubmit = document.getElementById('uploadbutton');

formSubmit.onclick = function(){
	return uploadfile();
}

function handleFileSelect(evt) {
	var fileInput = evt.target.files; // FileList object
	file = fileInput[0];

	var output = [];
	output.push('<strong>', escape(file.name), '</strong> (', file.type || 'n/a', ') <br>',
	            file.size, ' bytes<br>last modified: ',
	            file.lastModifiedDate ? file.lastModifiedDate.toLocaleDateString() : 'n/a'
	            );
	document.getElementById('list').innerHTML = output.join('');
	document.getElementById("uploadbutton").style.display = "inline";
  
	var textType = /text.*/;

	if (file.type.match(textType)) {
		var reader = new FileReader();

		reader.onload = function(e) {
			filecont = reader.result;
			Papa.parse(filecont, {
				header: true,
			    complete: function(results) {
					var filefields = results.meta.fields;
				    var ftable = document.getElementById("fieldlisttbl");
					var frows = ftable.rows.length;
					var srows = filefields.length
			
					for (r = 1; r < frows; r++) {
						selectlist = '';
						var fieldname = ftable.rows[r].cells[0].innerHTML;
						var selectlist = '<select id=\"'.concat(fieldname).concat('\">');

						var sampletable = '<b>sample file data</b><br><br><table class=\"table table-hover table-responsive\" id="sampletable"><tr>';

						for (s = 0; s < srows; s++) {
							var filefield = filefields[s];
							var selectlist = selectlist.concat('<option>').concat(filefield).concat('</option>');
							var sampletable = sampletable.concat('<th>').concat(filefield).concat('</th>');

						}
						selectlist = selectlist.concat('</select>');
						sampletable = sampletable.concat('</tr>');
						ftable.rows[r].cells[2].innerHTML = selectlist;
					}

					// load sample table						
					filedata = results.data
					var cols = filefields.length;
					if(filedata.length > 10) {filerows = 10;} else {filerows = filedata.length;}
					for (r = 0; r < filerows; r++) {
						for (c = 0; c < cols; c++) {
							sampletable = sampletable.concat('<td>').concat(filedata[r][filefields[c]]).concat('</td>');
						}
						sampletable = sampletable.concat('</tr>');
					}
					sampletable = sampletable.concat('</table>');
					document.getElementById("sampledata").innerHTML = sampletable;
			    }
			});
		}
			reader.readAsText(file);	
		} else {
			fileDisplayArea.innerText = "File not supported!";
		}		
}

document.getElementById('files').addEventListener('change', handleFileSelect, false);

function uploadfile() {
	var mapping = new Array();
    var ftable = document.getElementById("fieldlisttbl");
	var frows = ftable.rows.length;
	for (r = 1; r < frows; r++) {
		rowarray = [];
		rowarray[0] = ftable.rows[r].cells[0].innerHTML;
		var selectedField = document.getElementById(rowarray[0]).selectedIndex;
		var selectFields = document.getElementById(rowarray[0]).options;
		rowarray[1] = selectFields[selectedField].text;
		mapping[r-1] = rowarray;
	}

	var filedatas = JSON.stringify(filedata);
	var mappings = JSON.stringify(mapping);
	
	var dateformat = document.getElementById("dateformat").value;
	var connectindex = document.getElementById("datasource").selectedIndex;
	var connectlist = document.getElementById("datasource").options;
	var connectionid = connectlist[connectindex].value;
    $.post("../api/upload.php",
    {			
		filedata: filedatas,
		mapping: mappings,
		dateformat: dateformat,
		connectionid: connectionid
    },
    function(data,status){
//		loadingModal = document.getElementById("loadingModal");
//		loadingModal.style.display = "none";
//		loadingModal.setAttribute("aria-hidden",true);
//		loadingModal.setAttribute("data-dismiss","modal");
		alert(data);
    })
	
}

</script>

</body>

</html>
