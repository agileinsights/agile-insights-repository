<?php

//curl_setopt($curl, CURLOPT_URL, 'http://rwl-lstorma-p01:8080/schedule/rest/automation/query-for-object?ltk='.$token.'&sql='.$sql);
//$sql = 'from SimpleExecutionPlan where name = \'testapi\'';
//$sql = urlencode($sql);

$username = isset($_GET["username"]) ? $_GET["username"] : '';
$password = isset($_GET["password"]) ? $_GET["password"] : '';
$objectname = isset($_GET["objectname"]) ? $_GET["objectname"] : '';
$parameters = isset($_GET["parameters"]) ? $_GET["parameters"] : '';

// Get login token with cURL
$curl = curl_init();
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_URL, 'http://rwl-lstorma-p01:8080/login/rest');
curl_setopt($curl, CURLOPT_USERAGENT, 'Codular Sample cURL Request');
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, 'username='.$username.'&password='.$password);

$resp = curl_exec($curl);
$status = curl_getinfo($curl);

curl_close($curl);

$resp_json = json_decode($resp, true);
$token = $resp_json['message'];
$token = urlencode($token);

// Run execution plan

//$path = urlencode("deploy/executionPlan/testapi");
//$path = "deploy/executionPlan/testapi";
//$pathdata = array("path" => $path);                                                                         
//$pathdata_string = json_encode($pathdata);
$pathdata_string = '{"path":"deploy/executionPlan/testapi"}';

$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, 'http://rwl-lstorma-p01.dc.fmcna.com:8080/schedule/rest/automation/start-stop-run?ltk='.$token);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_USERAGENT, 'User-Agent: curl/7.27.0');
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_POSTFIELDS, $pathdata_string);
curl_setopt($curl, CURLOPT_HTTPHEADER, array(
	'Content-Type: text/plain',
	'Accept:text/plain',
	'Transfer-Encoding:chunked',
	'Server:Jetty(9.2.10.v20150310)'
    ));

$resp = curl_exec($curl);
$status = curl_getinfo($curl);
$error = curl_error($curl);

curl_close($curl);

$jsonresp = json_decode($resp);
//echo '<br><br>'.$error.'<br><br>';
echo '<pre>'.json_encode($jsonresp, JSON_PRETTY_PRINT).'</pre>';
//echo var_dump($status);

?>