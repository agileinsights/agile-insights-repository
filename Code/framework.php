<html>

<meta http-equiv="x-ua-compatible" content="IE=edge">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--
<!DOCTYPE html>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 -->
<head runat="server">
<title>Agile Insights</title>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="css/datepicker.css" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script src="../js/bootstrap-datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#startdate').datepicker({
            format: "yyyy-mm-dd"
        });
    });
</script>

  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
	  padding: 5px;
	  color: white;
	  }
    
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 100%}
    .row.content {overflow: auto}
    
    /* Set gray background color and 100% height */
    .sidenav {
      padding-top: 20px;
	  padding-bottom: 20px;
      background-color: #f1f1f1;
      height: 100%;
	  text-align: left;
	  overflow: auto
    }
    
    /* Set black background color, white text and some padding */
    footer {
      background-color: #555;
      color: white;
      padding: 5px;
    }
    
	#vizframe {
		display: block;
		margin: 10px;
	/*	width:  calc(100% - 235px); */
	/*	height: 100%; */
	/*	height: calc(100% - 120px);*/
	}
	
	.slider-span {
		display:none;
	}

</style>

</head>

<body>

<nav class="navbar navbar-inverse">
	<div id="container" class="container-fluid">
		<div class="navbar-header" id='header'>
			<h3>Agile Insights</h3>
		</div>
	</div>
</nav>

<div class="container-fluid text-center">    
	<div class="row content">
		<div class="col-sm-2 sidenav" id="nav">
		
			<input class="btn btn-primary" type="submit" value="Submit" onclick='gatherSelections();'><br>

			<br>output:<br>
			<select class="form-control" id='output' name='output'>
				<option value=Snapshot>visualization</option>
				<option value=dataset>dataset</option>
				<option value=calculator>what-if analysis</option>
				<option value=selections>selections</option>
				<option value=sql>sql</option>
				<option value=webservice>webservice</option>
			</select><br>
			
			program:<br>
<?php

$sid = $_GET["sid"];

	$db = '//hax-exa-scan:1521/KCNGXPS';

	$conn = oci_connect('BI_SRC', 'welcome', $db) or die('connection failed');

	if (!$conn) {
		 $e = oci_error();
		 trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
		echo('error');
	}
	
	// data source drop down list

    $query = 'select program_id, program_nm, hierarchy from program_fact order by program_nm';

	$stmt = oci_parse($conn, $query);
	oci_execute($stmt) or die('query failed');

    echo "<select class='form-control' id='sourcelist' name='sourcelist' onChange=window.location='framework.php?sid='+this.value>"; 
	
	while ($row = oci_fetch_array($stmt, OCI_BOTH)) {
		unset($program_id, $program_nm);
		$program_id = $row[0];
		$program_nm = $row[1];
		if($program_id == $sid) {
			$sel='selected';
			$hierarchy = $row[2];
			}
		echo '<option '.$sel.' value="'.$program_id.'">'.$program_nm.'</option>';
		$sel = '';
	}
	echo "</select><br>";
    echo "start date:<br><input type=\"text\" placeholder=\"click to select start date\" class=\"form-control\" id=\"startdate\"/>";
	
	// facility hierarchy drop down lists

    $query = 'select global_id, name, child_level, level_position, parent_global_id, parent_level, "level" from FAC_DIAL_HIERARCHY where hierarchy =  \''.$hierarchy.'\' and level_position = (select max(level_position) from FAC_DIAL_HIERARCHY where hierarchy =  \''.$hierarchy.'\') order by name';

	echo "<br><span id='hierarchy'>";
    echo "<br>division:<br>";

    echo "<select class='form-control' id='facilitylist' name='facilitylist' multiple='' class='ui fluid dropdown'>";

	$stmt = oci_parse($conn, $query);
	oci_execute($stmt) or die('query failed');

	while ($row = oci_fetch_array($stmt, OCI_BOTH)) {
		unset($global_id, $name, $child_level, $level_position);
		$global_id = $row[0];
		$name = $row[1];
		$child_level = $row[2];
		$level_position = $row[3];
		$parent_global_id = $row[4];
		$parent_level = $row[5];
		$current_level = $row[6];
		echo '<option data-level="'.$current_level.'" data-parentid="'.$parent_global_id.'" data-parent="'.$parent_level.'" data-child="'.$child_level.'" data-levelp="'.$level_position.'" value="'.$global_id.'">'.$name.'</option>'; 
	}
	echo "</select>";
	echo "</span><br>";
	
	echo "<button class=\"btn btn-primary\" id=\"drilldown\" onclick=\"hierarchyDrillDown('".$hierarchy."');\" style=\"margin-top:10px;\" value=\"drill down\">
			drill <span class=\"glyphicon glyphicon-arrow-down\"></span>
		  </button>&nbsp;&nbsp;&nbsp;";
	echo "<button class=\"btn btn-primary\" id=\"drillup\" type=\"button\" onclick=\"hierarchyDrillUp('".$hierarchy."');\" style=\"margin-top:10px;\" value=\"drill up\">
			drill <span class=\"glyphicon glyphicon-arrow-up\"></span>
		  </button>";
	
	// metric drop down list

    $query = 'select program_id, metric_cd, metric_label
				from metric_data_dim m 
				inner join program_metric_dim_fact p
				on m.metric_id = p.metric_dim_id 
				and p.fact_type = \'metric\' 
				and p.program_id = '.$sid.' 
				order by metric_cd';
    echo "<br><br>metrics:<br>";
    echo "<select class='form-control' id='metriclist' name='metriclist' multiple='' class='ui fluid dropdown' onchange='showSliders();'>";

	$stmt = oci_parse($conn, $query);
	oci_execute($stmt) or die('query failed');

	$slider_html = '';
	
	while ($row = oci_fetch_array($stmt, OCI_BOTH)) {
		unset($source_id, $source_nm);
		$program_id = $row[0];
		$metric_cd = $row[1];
		$metric_label = $row[2];
		echo '<option value="'.$metric_cd.'">'.$metric_label.'</option>';
		
		$slider_html = $slider_html.'<span class="slider-span" id="'.$metric_cd.'_span"><br>'.
		$metric_label.'<br><input type="range" id="'.$metric_cd.'_slider" class="slider-width100" name="points" min="0" max="1" value="0.5" step="0.01" onchange="showValue(this.value, this.id)" >
			<span id="'.$metric_cd.'_slider_val" name="'.$metric_cd.'_slider_val">0.5</span><br></span>
		';
		
	}
	echo "</select>";
	echo '<br>'.$slider_html.
	'	<script type="text/javascript">
			function showValue(newValue, sliderId)
				{
					document.getElementById(sliderId + "_val").innerHTML=newValue;
				}
		</script>';
	
	// group by list

    $query = 'select distinct DIMENSION_CD
				from dimension_data_dim d
				inner join program_metric_dim_fact p
				on d.dimension_id = p.metric_dim_id 
				and p.fact_type = \'dimension\' 
				and p.program_id = '.$sid.' 
				order by DIMENSION_CD';
	
    echo "<br><br>group by:<br>";
	
	$stmt = oci_parse($conn, $query);
	oci_execute($stmt) or die('query failed');

	echo $dimension_cd."<select class='form-control' id='groupby' name='groupby' class='ui fluid dropdown'>";

	echo '<option value="Hierarchy">Hierarchy</option>';
	echo '<option value="0">Facility</option>';
	echo '<option value="1">Area</option>';
	echo '<option value="2">Region</option>';
	echo '<option value="3">Operating Group</option>';
	while ($row = oci_fetch_array($stmt, OCI_BOTH)) {
		$dimension_cd = $row[0];
		echo '<option value="'.$dimension_cd.'">'.$dimension_cd.'</option>';
	}
	echo "</select>";

	// dimension drop down lists

    $query = 'select distinct program_id, DIMENSION_COMBO_ID, DIMENSION_CD, DIMENSION_VALUE
				from dimension_data_dim d
				inner join program_metric_dim_fact p
				on d.dimension_id = p.metric_dim_id 
				and p.fact_type = \'dimension\' 
				and p.program_id = '.$sid.' 
				order by DIMENSION_CD';
    echo "<br><br>dimension filters:<br>";

	$stmt = oci_parse($conn, $query);
	oci_execute($stmt) or die('query failed');

	$cur_dimension_cd = '';
	
	while ($row = oci_fetch_array($stmt, OCI_BOTH)) {

		unset($source_id, $dimension_cd);
		$program_id = $row[0];
		$dimension_combo_id = $row[1];
		$dimension_cd = $row[2];
		$dimension_value = $row[3];
		if ($dimension_cd != $cur_dimension_cd) {
			if ($cur_dimension_cd != '') {
				echo "</select><br>";  // close out previous select
			}
			echo $dimension_cd.":<br><select class='form-control' id='".$dimension_cd."' name='dimensionlist_".$dimension_cd."' multiple='' class='ui fluid dropdown'>";
			$cur_dimension_cd = $dimension_cd;
		}
		echo '<option value="'.$dimension_combo_id.'">'.$dimension_value.'</option>';
	}
	echo "</select><br>";
?>
			<br>
		</div>
		<div class="col-sm-10 text-left"> 
			<div id="rawcontent"></div>
			<iframe class="embed-responsive-item" name='vizframe' id='vizframe' frameborder='0' height='100%' width='100%'></iframe>
		</div>
	</div>
</div>

<footer class="container-fluid text-center" id='footer'>
	<span id='breadcrumb'>selections:<br></span>
</footer>

</body>
</html>

<!--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
-->
<script>

	function showSliders() {
	    var selectBox = document.getElementById("metriclist");
		listlength = selectBox.options.length;
		
		var output = document.getElementById("output");
		var selectedoutput = output.options[output.selectedIndex].value;
			
		for(var i = 0; i < listlength; i++){
			selectedMetric = selectBox.options[i].value;
			var sliderSpan = document.getElementById(selectedMetric + "_span");
			if(selectBox.options[i].selected && selectedoutput == 'calculator') {
				sliderSpan.style.display = 'inline';
				}
			else {
				sliderSpan.style.display = 'none';
				}
			}
	}
	
	function gatherSelections() {
		var selectTags = document.getElementsByTagName('select');
		var outputtag = document.getElementById("output");
		var output = outputtag.options[outputtag.selectedIndex].value;
				
		var DimItms = '';
		var MetricItms = '';
		var DimComboItms = '';
		var GroupBy = '';
		var sliderParams = '';
		var startdate = '';
		
		DimItms = DimItms.concat('"Dimensions":[');
		MetricItms = MetricItms.concat('"Metrics":');		
		DimComboItms = DimComboItms.concat('"DimCombo":[');
		GroupBy = GroupBy.concat('"GroupBy":');
		sliderParams = sliderParams.concat('"MetricParams":[');
		startdate = startdate.concat('"startdate":');
		
		for (var tg = 0; tg < selectTags.length; tg++) {
			var tag = selectTags[tg];
			var tagid = document.getElementById(tag.id);
			listlength = tagid.options.length;
			
			// get selected dimensions
			if(tagid.name.match('dimension')) {
				DimItmsTemp = '"'.concat(tagid.id).concat('",');  //.concat('":["');
				DimComboItms = DimComboItms.concat('');
				selectedItems = 0;
				for(var i = 0; i < listlength; i++){
				  if(tagid.options[i].selected) {
						selectedItems++;
						SelDim = tagid.options[i].text.replace(/"/g, '\'')
					//	DimItmsTemp = DimItmsTemp.concat(SelDim).concat('","');
						DimComboItms = DimComboItms.concat(tagid.options[i].value).concat(',');
						}
				}
				if (selectedItems > 0) {
					DimItms = DimItms.concat(DimItmsTemp);
				//	DimItms = DimItms.slice(0, -2).concat(']},');
					DimItmsTemp = '';
				}
				DimComboItms = DimComboItms.slice(0, -1).concat(',');
			}
			// get selected metrics
			else if(tagid.name.match('metric')) {
				MetricItms = MetricItms.concat('["');
				for(var i = 0; i < listlength; i++){
				  if(tagid.options[i].selected) {
						SelMet = tagid.options[i].value.replace(/"/g, '\'');
						MetricItms = MetricItms.concat(SelMet).concat('","');
						selParam = document.getElementById(SelMet + "_slider_val").innerHTML;
						sliderParams = sliderParams.concat(selParam).concat(',');
						}
				}
			MetricItms = MetricItms.slice(0, -2).concat('],');
			sliderParams = sliderParams.slice(0, -1).concat('],');
			}
						
			// get selected Group By
			else if(tagid.name.match('groupby')) {
				GroupBy = GroupBy.concat('["');
				for(var i = 0; i < listlength; i++){
				  if(tagid.options[i].selected) {
						SelGB = tagid.options[i].text.replace(/"/g, '\'')
						GroupBy = GroupBy.concat(SelGB).concat('","');
						}
				}
			GroupBy = GroupBy.slice(0, -2).concat(']');
			}

		}
		
		// get selected hierarchy level and values
		var fld = document.getElementById('facilitylist');
		var selectarray = [];
		for (var i = 0; i < fld.options.length; i++) {
		  if (fld.options[i].selected) {
			selectarray.push(fld.options[i].value);
			level_position = fld.options[i].getAttribute('data-levelp');
		  }
		}
		var hierarchy_selections = selectarray.toString();
		hierarchy_selections = '\"'.concat(hierarchy_selections.replace(/,/g, '\",\"')).concat('\"');
		
	DimItms = DimItms.slice(0, -1).concat(']');

	if(DimItms=='\"Dimensions\":[}]' || DimItms=='\"Dimensions\":]') {DimItms='\"Dimensions\":[]'};

		MetricItms = MetricItms.slice(0, -1);
		
		sliderParams = sliderParams.slice(0, -1);
		
		sd = document.getElementById("startdate").value
		startdate = startdate.concat('"').concat(sd).concat('"');

		DimComboItms = DimComboItms.slice(0, -1).concat(']');
		if(DimComboItms=='\"DimCombo\":]') {DimComboItms='\"DimCombo\":[0]'};

		SelItems = '{'.concat(MetricItms).concat(',').concat(DimItms).concat(',').concat(DimComboItms).concat(',"level_position":').concat(level_position)
			.concat(',"hierarchy_selections":[').concat(hierarchy_selections).concat('],')
			.concat(GroupBy)
			.concat(',').concat(startdate)
			.concat(',').concat(sliderParams)
			.concat('}');
			
		SelItems = SelItems.replace(/&/g, 'and');
		
//		var uri = 'http://biprotodev01/setrequest.php?selections='.concat(SelItems).concat('&output=').concat(output);
//		var url = encodeURI(uri);
		
		$.post("../setrequest.php",
          {			
  			selections: SelItems,
			output: output
			},
          function(data,status){
			  if (output == 'selections' || output == 'sql') {
				  document.getElementById("rawcontent").innerHTML = data;
			  }
			  else {
				//  document.getElementById("rawcontent").innerHTML = data;
				document.getElementById('vizframe').src = data;				  
			  }
          })

	}
	
	// functions to drill up and down organizational hierarchies
	function hierarchyDrillDown(hierarchy) {
		
		var fld = document.getElementById('facilitylist');
		var selectarray = [];
		var selectnamearray = [];
		for (var i = 0; i < fld.options.length; i++) {
		  if (fld.options[i].selected) {
			selectarray.push(fld.options[i].value);
			selectnamearray.push(fld.options[i].text);
			child_level = fld.options[i].getAttribute('data-child');
			current_level = fld.options[i].getAttribute('data-level');
		  }
		}
		var selections = selectarray.toString();
		var selectionnames = selectnamearray.toString();
		var level = child_level;
		var direction = 'down';
		
		$.post("../facility_hierarchy.php",
          {			
  			selections: selections,
			level: level,
			direction: direction,
			hierarchy: hierarchy
          },
          function(data,status){
  			document.getElementById("hierarchy").innerHTML = data;
          })
		  
  		if (level == 'facility') {
			document.getElementById('drilldown').style.display = 'none';
			document.getElementById('drillup').style.display = 'inline';
		}
		else {
			document.getElementById('drilldown').style.display = 'inline';
			document.getElementById('drillup').style.display = 'inline';
		}
		
		var breadcrumb = document.getElementById('breadcrumb').innerHTML;
		breadcrumb = breadcrumb.concat(current_level).concat(': ').concat(selectionnames).concat("<br>");
		document.getElementById('breadcrumb').innerHTML = breadcrumb;
	}

	function hierarchyDrillUp(hierarchy) {
		
		var fld = document.getElementById('facilitylist');
		var selectarray = [];
		for (var i = 0; i < fld.options.length; i++) {
			selectarray.push(fld.options[i].getAttribute('data-parentid'));
			parent_level = fld.options[i].getAttribute('data-parent');
		}

		var selections = selectarray.toString();
		var level = parent_level;
		var direction = 'up';
		
		$.post("../facility_hierarchy.php",
          {			
  			selections: selections,
			level: level,
			direction: direction,
			hierarchy: hierarchy
			},
          function(data,status){
  			document.getElementById("hierarchy").innerHTML = data;
//			alert(data);
          })
		if (level == 'division') {
			document.getElementById('drillup').style.display = 'none';
			document.getElementById('drilldown').style.display = 'inline';
		}
		else {
			document.getElementById('drillup').style.display = 'inline';
			document.getElementById('drilldown').style.display = 'inline';
		}
	}
	
</script>
