<?php

date_default_timezone_set("GMT");
$curdatetime = new DateTime(date('Y-m-d h:i:s'));
$curdatetime_str = $curdatetime->format('Y-m-d H:i:s');

$selections = $_POST["selections"];
$output = $_POST["output"];

$selectionsjson = json_decode($selections);
$metriclist = '\''.implode('\',\'', $selectionsjson->{'Metrics'}).'\'';
$metricparams = implode('\',\'', $selectionsjson->{'MetricParams'});

$startdate = $selectionsjson->{'startdate'};

$metricparamarray = array($selectionsjson->{'Metrics'}, $selectionsjson->{'MetricParams'});

$dimensionarray = $selectionsjson->{'Dimensions'};
$dimensionCount = count($dimensionarray);

$paramtable = '';
for ($i = 0; $i < sizeof($metricparamarray[1]); $i++) {
	$paramtable =  $paramtable.'select \''.$metricparamarray[0][$i].'\' as metric_cd, '.$metricparamarray[1][$i].' as metric_param from dual
			union ';
}
$paramtable = rtrim($paramtable, 'union ');

//$metricparams = $selectionsjson->{'MetricParams'};
$dimcombolist = implode(',', $selectionsjson->{'DimCombo'});
//$clinicid = implode(',', $selectionsjson->{'clinicID'});
$hierarchy_selections = implode(',', $selectionsjson->{'hierarchy_selections'});
// $hierarchy_selections = '\''.implode('\',\'', $selectionsjson->{'hierarchy_selections'}).'\'';

$level_position = $selectionsjson->{'level_position'};
$groupby = '\''.implode('\',\'', $selectionsjson->{'GroupBy'}).'\''; //$selectionsjson->{'GroupBy'};

$user = 'anthony';

$requestid = uniqid();

if ($groupby == '\'Hierarchy\'') {
	$sql_group_by = '
		(
					select fdh.global_id as facility_id, fdh2."level" GROUPBY1_CD, fdh2.name GROUPBY1_VALUE
					FROM FAC_DIAL_HIERARCHY fdh
					inner join FAC_DIAL_HIERARCHY fdh2
							on fdh2.level_position = '.$level_position.' - 1 
											and fdh2.parent_global_id in ('.$hierarchy_selections.')
							and hierlevel(fdh.bit_id, '.$level_position.' - 1, 6) = fdh2.global_id
							and fdh."level" = \'facility\'
			) d on d.facility_id = m.parent_id							
	';
}

// this block needs to be updated to include a third table alias to join in the group by level
//elseif ($groupby >= 0 || $groupby <= 3) {
//	$sql_group_by = '
//		(
//					select fdh.global_id as facility_id, fdh2."level" GROUPBY1_CD, fdh2.name GROUPBY1_VALUE
//					FROM FAC_DIAL_HIERARCHY fdh
//					inner join FAC_DIAL_HIERARCHY fdh2
//							on fdh2.level_position = '.$groupby.' 
//											and fdh2.parent_global_id in ('.$hierarchy_selections.')
//							and hierlevel(fdh.bit_id, '.$groupby.', 6) = fdh2.global_id
//							and fdh."level" = \'facility\'
//			) d on d.facility_id = m.parent_id							
//	';
//}

else {
	$sql_group_by = '
		(
			select subject_id, post_date, GROUPBY1_CD, GROUPBY1_VALUE
			from (
					select distinct ddf.subject_id, ddf.post_date, DIMENSION_CD GROUPBY1_CD, DIMENSION_VALUE GROUPBY1_VALUE, ddf.PARENT_ID
					FROM DIMENSION_DATA_FACT ddf inner join DIMENSION_DATA_DIM ddd
						on ddf.DIMENSION_COMBO_ID = ddd.DIMENSION_COMBO_ID
						and ddd.DIMENSION_CD in ('.$groupby.')
					inner join FAC_DIAL_HIERARCHY f
					on ddf.PARENT_ID = f.global_id
					and f."level" = \'facility\'
					and hierlevel(f.bit_id, '.$level_position.', 6) in ('.$hierarchy_selections.'))
			) d on d.subject_id = m.subject_id and d.post_date = m.post_date	
	';
}

/*
	$sql_group_by = '
					select distinct ddf.subject_id, ddf.post_date, fdh2."level" GROUPBY1_CD, fdh2.name GROUPBY1_VALUE, ddf.PARENT_ID
					FROM DIMENSION_DATA_FACT ddf inner join FAC_DIAL_HIERARCHY fdh
						on ddf.parent_id = fdh.id
						and fdh."level" = \'facility\'
					inner join FAC_DIAL_HIERARCHY fdh2
							on fdh2.level_position = '.$level_position.' - 1 
											and fdh2.parent_global_id in ('.$hierarchy_selections.')
							and hierlevel(fdh.bit_id, '.$level_position.' - 1, 6) = fdh2.global_id)
	';
}
*/


if ($dimcombolist=='0') {
	$sql_dim_filter = '';
}
else {
	$sql_dim_filter = '
	(
			select subject_id, post_date
			FROM (
				select distinct subject_id, post_date, PARENT_ID
				FROM DIMENSION_DATA_FACT 
				where DIMENSION_COMBO_ID in ('.$dimcombolist.')
				and post_date >= to_date(\''.$startdate.'\', \'YYYY-MM-DD\')
				group by subject_id, post_date, PARENT_ID
				having count(subject_id) >= '.$dimensionCount.'
			) df inner join FAC_DIAL_HIERARCHY f
				on df.PARENT_ID = f.global_id
				and f."level" = \'facility\'
				and hierlevel(f.bit_id, '.$level_position.', 6) in ('.$hierarchy_selections.')
	) df on m.subject_id = df.subject_id and m.post_date = df.post_date
	inner join
';
}


$sql_string = '
	select m.post_date, mdd.METRIC_CD, mdd.metric_calc, mdd.metric_group, m.metric_rank, mdd.metric_description, mdd.metric_label, mp.metric_param,
	mdd.metric_prefix, mdd.metric_suffix, mdd.metric_scale, mdd.metric_up_meaning, mdd.metric_decimal, 
	mdd.metric_goal_low, mdd.metric_goal_med, mdd.metric_goal_high, mdd.metric_source, GROUPBY1_CD, GROUPBY1_VALUE,
	sum(case when m.metric_position = \'numerator\' and m.metric_type = \'Actual\' then nvl(metric_value,0) else 0 end) as numerator,
	sum(case when m.metric_position = \'denominator\' and m.metric_type = \'Actual\' then nvl(metric_value,0) else 0 end) as denominator,
	sum(case when m.metric_position = \'numerator\' and m.metric_type = \'Goal\' then nvl(metric_value,0) else 0 end) as numerator_goal,
	sum(case when m.metric_position = \'denominator\' and m.metric_type = \'Goal\' then nvl(metric_value,0) else 0 end) as denominator_goal,
	max(case when m.metric_type = \'Goal\' then 1 else 0 end) goal_flag
	from (
		'.$paramtable.'
		) mp INNER JOIN METRIC_DATA_DIM mdd 
		ON mp.metric_cd = mdd.metric_cd inner join 
	
	-- metrics
	(
			select subject_id, m.PARENT_ID, post_date, ml.metric_cd, m.metric_cd as metric_part, metric_position, 
				metric_calc, metric_type, metric_group, metric_rank, metric_value
			from metric_data_fact m inner join FAC_DIAL_HIERARCHY f
				on m.PARENT_ID = f.global_id
				and f."level" = \'facility\'
				and hierlevel(f.bit_id, '.$level_position.', 6) in ('.$hierarchy_selections.')
				and post_date >= to_date(\''.$startdate.'\', \'YYYY-MM-DD\')
				
				inner join (
				
						select metric_cd, METRIC_part, metric_position,
							metric_calc, metric_type, metric_group, DENSE_RANK() OVER (ORDER BY metric_group) metric_rank
						from (				
							select metric_cd, metric_numerator as METRIC_part, \'numerator\' as metric_position, 
								metric_calc, metric_type, metric_group
							FROM METRIC_DATA_DIM where metric_cd in ('.$metriclist.')
							union 
							select metric_cd, metric_denominator as METRIC_part, \'denominator\' as metric_position, 
								metric_calc, metric_type, metric_group
							FROM METRIC_DATA_DIM where metric_cd in ('.$metriclist.') 
							and metric_denominator <> \'1\'
						) mr
					) ml on m.METRIC_CD = ml.metric_part								
	) m on mdd.metric_cd = m.metric_group inner join 
	
	-- dimension filters
	'.$sql_dim_filter.'
	
	-- group by dimensions
	'.$sql_group_by.'
	group by m.post_date, mdd.METRIC_CD, mdd.metric_calc, mdd.metric_group, m.metric_rank, mdd.metric_description, mdd.metric_label, mp.metric_param, 
	mdd.metric_prefix, mdd.metric_suffix, mdd.metric_scale, mdd.metric_up_meaning, mdd.metric_decimal, 
	mdd.metric_goal_low, mdd.metric_goal_med, mdd.metric_goal_high, mdd.metric_source, GROUPBY1_CD, GROUPBY1_VALUE
';

$sql_string = str_replace("'","''",$sql_string);

$db = '//hax-exa-scan:1521/KCNGXPS';
$conn = oci_connect('BI_SRC', 'welcome', $db) or die('connection failed');

if (!$conn) {
     $e = oci_error();
     trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
	echo('error');
}

$insertsql = 'insert into DIAL_SQL_REQUEST_FACT(REQUEST_ID, SQL_STRING, INSERT_DATE, INSERT_BY)
	values(\''.$requestid.'\', \''.$sql_string.'\', current_date, \''.$user.'\')';

$stmt = oci_parse($conn, $insertsql);
oci_execute($stmt, OCI_COMMIT_ON_SUCCESS) or die('insert failed');


if ($output == 'selections') {
	echo('Success! Request '.$requestid.' created at '.$curdatetime_str.'<br>');
	echo '<pre>'.json_encode($selectionsjson, JSON_PRETTY_PRINT).'</pre>';
}

else if ($output == 'sql') {
	echo $requestid;
	echo '<pre>'.$sql_string.'</pre>';
}

else if ($output == 'webservice') {
	$targetpage = 'http://10.12.35.92/datasource.php?request_id='.$requestid;
	echo $targetpage;

//	header( 'Location: '.$targetpage );
}

else if ($output == 'dataset') {
//	$targetpage = 'http://tabappqa02/authoring/PerformanceMonitoring/'.$output.'?:embed=y&pRequestID='.$requestid;
	$targetpage = 'http://tabappoc01/authoring/PerformanceMonitoring/'.$output.'?:embed=y&pRequestID='.$requestid;
	echo $targetpage;
//	header( 'Location: '.$targetpage );
}

else {
//	$targetpage = 'http://tabappqa02.corp.ad.fmcna.com/views/PerformanceMonitoring/'.$output.'?:embed=y&:showShareOptions=true&:display_count=no&:showVizHome=no&pRequestID='.$requestid;
//	$targetpage = 'http://tabappqa02/views/PerformanceMonitoring/'.$output.'?:embed=y&pRequestID='.$requestid;	
	$targetpage = 'http://tabappoc01/views/PerformanceMonitoring/'.$output.'?:embed=y&pRequestID='.$requestid;	
	echo $targetpage;
//	header( 'Location: '.$targetpage );
}




?>
