--------------------------------------------------------
--- crate pipeline function to build dynamic queries ---
--------------------------------------------------------

-- define an object type

drop TYPE metricDataObject_ntt;

create or replace TYPE metricDataObject AS OBJECT (

   	post_date DATE,
   	hierarchy_label VARCHAR2(225),
   	hierarchy_id VARCHAR2(25),
   	hierarchy_level VARCHAR2(25),
   	metric_total VARCHAR2(225),
   	metric_category VARCHAR2(225),
   	metric_group VARCHAR2(225),
   	metric_label VARCHAR2(225),
	metric_id number(18,6),
   	metric_modality VARCHAR2(225),
	metric_goal_low number(18,6),
	metric_goal_med number(18,6),
	metric_goal_high number(18,6),
	metric_scale number(18,6),
	metric_up_meaning number(18,6),
	metric_numerator number(18,6),
	metric_denominator number(18,6),
	metric_value number(18,6),
	ultrascore number(18,6),
	standardized_score number(18,6),
	possible_score number(18,6),
	cqs_score number(18,6),
	cqs_flag number(18,6),
	us_flag number(18,6)

   );

-- define type "table" of MyObjects
create TYPE metricDataObject_ntt IS TABLE OF metricDataObject ;

--/
    create or replace FUNCTION get_metricDataObjects(
        p_filter_global_id VARCHAR2,
        p_filter_hierarchy_level INT,
        p_hierarchy_name VARCHAR2,
        p_start_date VARCHAR2,
        p_end_date VARCHAR2,
        p_grouping_hierarchy_level INT,
        p_modality VARCHAR2,
        p_grouping_metric_level INT,
        p_scenario VARCHAR2
    
    ) RETURN metricDataObject_ntt 
    PIPELINED
    PARALLEL_ENABLE
    IS
        cur sys_refcursor ;

   	post_date DATE ;
   	hierarchy_label VARCHAR2(225) ;
   	hierarchy_id VARCHAR2(25) ;
   	hierarchy_level VARCHAR2(25) ;
   	metric_total VARCHAR2(225) ;
   	metric_category VARCHAR2(225) ;
   	metric_group VARCHAR2(225);
   	metric_label VARCHAR2(225) ;
	metric_id number(18,6);
   	metric_modality VARCHAR2(225);
	metric_goal_low number(18,6); 
	metric_goal_med number(18,6); 
	metric_goal_high number(18,6); 
	metric_scale number(18,6) ;
	metric_up_meaning number(18,6) ;
	metric_numerator number(18,6) ;
	metric_denominator number(18,6) ;
	metric_value number(18,6) ;
	ultrascore number(18,6) ;
	standardized_score number(18,6) ;
	possible_score number(18,6) ;
	cqs_score number(18,6) ;
	cqs_flag number(18,6) ;
	us_flag number(18,6) ;
	
        sqlrequest varchar2(20000) := '
        
        -- Detailed view --
        with metric_detail as (
        select 
                f.name as hierarchy_label,
                f.id as hierarchy_id,
                f."level" as hierarchy_level,
                m.post_date, 
                m.METRIC_ID,
                m.metric_rank, 
                m.fac_id,
                md.METRIC_CD,
                md.metric_calc, 
                ' || p_grouping_metric_level || ' as metric_level,
                md.METRIC_CATEGORY,
                md.metric_group, 
                md.METRIC_LABEL,
                md.metric_modality, 
                nvl(sc.cqs_flag, md.cqs_flag) cqs_flag, 
                md.us_flag, 
                md.us_deduction,
                case when md.METRIC_UP_MEANING = ''good'' then 1 else -1 end as METRIC_UP_MEANING,
                md.METRIC_SCALE,
                nvl(round(sc.METRIC_GOAL_LOW_S, 5), round(md.METRIC_GOAL_LOW, 5)) METRIC_GOAL_LOW,
                nvl(round(sc.METRIC_GOAL_MED_S, 5), round(md.METRIC_GOAL_MED, 5)) METRIC_GOAL_MED,
                nvl(round(sc.METRIC_GOAL_HIGH_S, 5), round(md.METRIC_GOAL_HIGH, 5)) METRIC_GOAL_HIGH,
        
        sum(case when m.metric_position = ''numerator'' then nvl(metric_value,0) else 0 end) as numerator,
        sum(case when m.metric_position = ''denominator'' then nvl(metric_value,0) else 0 end) as denominator
        from 
        (
                select to_number(subject_id) as fac_id, f.bit_id, post_date, ml.metric_id, ml.metric_calc, ml.metric_modality, ml.metric_group, metric_position, metric_rank, metric_value, ml.cqs_flag, ml.us_flag, ml.us_deduction
                from metric_data_fact m inner join (
                        select metric_id, mr.metric_calc, mr.metric_modality, mr.metric_group, METRIC_part, metric_position, DENSE_RANK() OVER (ORDER BY metric_id) metric_rank, cqs_flag, us_flag, us_deduction
                        from (				
                                select metric_id, metric_calc, metric_modality, metric_group, metric_numerator as METRIC_part, ''numerator'' as metric_position, cqs_flag, us_flag, us_deduction
                                FROM METRIC_DATA_DIM 
                                where (cqs_flag = 1 or us_flag = 1)
                                and metric_calc = ''Rate''
                                and metric_modality in (' || p_modality || ')
                                union 
                                select metric_id, metric_calc, metric_modality, metric_group, metric_denominator as METRIC_part, ''denominator'' as metric_position, cqs_flag, us_flag, us_deduction
                                FROM METRIC_DATA_DIM 
                                where (cqs_flag = 1 or us_flag = 1)
                                and metric_calc = ''Rate''
                                and metric_denominator <> ''1''
                                and metric_modality in (' || p_modality || ')
                        ) mr
                ) ml on m.METRIC_CD = ml.metric_part
                and post_date >= trunc(to_date(''' || p_start_date || ''', ''YYYY-MM-DD''), ''MONTH'')
                and post_date < trunc(to_date(''' || p_end_date || ''', ''YYYY-MM-DD'') + 1, ''MONTH'')
                
                inner join FAC_DIAL_HIERARCHY f 
                on m.subject_id = f.global_id
                and f."level" = ''facility''

                and hierlevel(f.bit_id, ' || p_filter_hierarchy_level || ', 6) in (' || p_filter_global_id || ')
                and hierarchy = ''' || p_hierarchy_name || '''
                
        ) m inner join metric_data_dim md on 
                m.metric_id = md.metric_id
            left join (
                        SELECT st.*
                        from (
                                select
                                case when ''' || p_scenario || ''' = ''''
                                    then ''{"rows": [{"id": "0","name": "0","current": [0,0,0],"new": [0,0,0]}]}''
                                    else ''' || p_scenario || ''' end as scenario
                                from dual
                        
                        ) sdata,
                        JSON_TABLE(scenario, ''$.rows[*]''
                        COLUMNS (
                                METRIC_ID VARCHAR2(20) PATH ''$.id'',
                                CQS_FLAG VARCHAR2(20) PATH ''$.cqs_flag'',
                                METRIC_GOAL_LOW_S VARCHAR2(20) PATH ''$.new[0]'',
                                METRIC_GOAL_MED_S VARCHAR2(20) PATH ''$.new[1]'',
                                METRIC_GOAL_HIGH_S VARCHAR2(20) PATH ''$.new[2]''
                        )) AS st
                        ) sc
             on md.metric_id = sc.metric_id
                
          inner join FAC_DIAL_HIERARCHY f 
                on hierlevel(m.bit_id, ' || p_grouping_hierarchy_level || ', 6) = to_number(f.global_id)
                and f.level_position = ' || p_grouping_hierarchy_level || '
                and hierarchy = ''' || p_hierarchy_name || '''

        group by         
                f.name,
                f.id,
                f."level",
                m.post_date, 
                m.METRIC_ID,
                m.metric_rank, 
                m.fac_id,
                md.METRIC_CD,
                md.metric_calc, 
                md.METRIC_CATEGORY,
                md.metric_group,
                md.METRIC_LABEL,
                md.metric_modality,
                nvl(sc.cqs_flag, md.cqs_flag), 
                md.us_flag, 
                md.us_deduction,
                case when md.METRIC_UP_MEANING = ''good'' then 1 else -1 end,
                md.METRIC_SCALE,
                nvl(round(sc.METRIC_GOAL_LOW_S, 5), round(md.METRIC_GOAL_LOW, 5)),
                nvl(round(sc.METRIC_GOAL_MED_S, 5), round(md.METRIC_GOAL_MED, 5)),
                nvl(round(sc.METRIC_GOAL_HIGH_S, 5), round(md.METRIC_GOAL_HIGH, 5))
        )
        
        -- Summarized calculations --
     select 
        trunc(post_date, ''MONTH'') post_date,
        hierarchy_label,
        hierarchy_id,
        hierarchy_level,
        ''Total'' as metric_total,
        case when metric_level = 1 then ''Total'' 
                else metric_category
                end as metric_category, 
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                else metric_group 
                end as metric_group,
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                when metric_level = 3 then metric_group
                else metric_label 
                end as metric_label,
        case when metric_level = 1 then 0 
                when metric_level = 2 then 0
                when metric_level = 3 then 0
                else metric_id 
                end as metric_id,
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                when metric_level = 3 then metric_group
                else metric_modality 
                end as metric_modality,

        case when metric_level < 4 then null else METRIC_GOAL_LOW end as METRIC_GOAL_LOW,
        case when metric_level < 4 then null else METRIC_GOAL_MED end as METRIC_GOAL_MED,
        case when metric_level < 4 then null else METRIC_GOAL_HIGH end as METRIC_GOAL_HIGH,
        case when metric_level < 3 then null else METRIC_SCALE end as METRIC_SCALE,
        case when metric_level < 3 then null else METRIC_UP_MEANING end as METRIC_UP_MEANING,
        case when metric_level < 3 then sum(0) else sum(metric_numerator) end metric_numerator, 
        case when metric_level < 3 then sum(0) else sum(metric_denominator) end metric_denominator, 
        case when metric_level < 3 then sum(0) else 
                decode(sum(metric_denominator), 0, 0, sum(metric_numerator)/sum(metric_denominator)) end metric_value,        
        case 
                when sum(case when us_deduction > 0 then 1 else 0 end) < 5 and metric_level < 3 then 0 
                else sum(metric_value * us_deduction * us_flag * 100) end ultrascore,
        sum(standardized_score * cqs_flag) as standardized_score,
        sum(possible_score) as possible_score,
        decode(sum(possible_score), 0, null, sum(standardized_score * cqs_flag) / sum(possible_score) * 100) as cqs_score,
        max(cqs_flag) as cqs_flag, 
        max(us_flag) as us_flag
        from (

        select 
                post_date,
                hierarchy_label,
                hierarchy_id,
          	hierarchy_level,
          	metric_level,
                metric_calc,
                metric_id,
                metric_category, 
                metric_group,
                metric_label,
                metric_modality,
                METRIC_GOAL_LOW,
                METRIC_GOAL_MED,
                METRIC_GOAL_HIGH,
                METRIC_SCALE,
                cqs_flag, 
                us_flag, 
                us_deduction,
                METRIC_UP_MEANING,
                sum(numerator) metric_numerator, 
                sum(denominator) metric_denominator, 
                decode(sum(denominator), 0, null, sum(numerator)/sum(denominator)) metric_value,
                decode(sum(denominator), 0, null,
                case 
                        when METRIC_SCALE * METRIC_UP_MEANING * sum(numerator)/sum(denominator) < METRIC_UP_MEANING * METRIC_GOAL_LOW then 
                                0
                        when METRIC_SCALE * METRIC_UP_MEANING * sum(numerator)/sum(denominator) < METRIC_UP_MEANING * METRIC_GOAL_MED then 
                                cast(4 * ((METRIC_SCALE * METRIC_UP_MEANING * sum(numerator)/sum(denominator) - METRIC_UP_MEANING * METRIC_GOAL_LOW) / 
                                        (METRIC_UP_MEANING * METRIC_GOAL_MED - METRIC_UP_MEANING * METRIC_GOAL_LOW)) + 1 as int)
                        when METRIC_SCALE * METRIC_UP_MEANING * sum(numerator)/sum(denominator) < METRIC_UP_MEANING * METRIC_GOAL_HIGH then 
                                cast(5 * ((METRIC_SCALE * METRIC_UP_MEANING * sum(numerator)/sum(denominator) - METRIC_UP_MEANING * METRIC_GOAL_MED) / 
                                        (METRIC_UP_MEANING * METRIC_GOAL_HIGH - METRIC_UP_MEANING * METRIC_GOAL_MED)) + 5 as int)
                        else 10 end) as standardized_score,
                10 as possible_score
                
        from METRIC_DETAIL
        group by
                post_date,
                hierarchy_label,
                hierarchy_id,
          	hierarchy_level,
          	metric_level,
                metric_calc,
                metric_id,
                metric_category, 
                metric_group,
                metric_label,
                metric_modality,
                METRIC_GOAL_LOW,
                METRIC_GOAL_MED,
                METRIC_GOAL_HIGH,
                METRIC_SCALE,
                cqs_flag, 
                us_flag, 
                us_deduction,
                METRIC_UP_MEANING
                ) mg
     group by
        trunc(post_date, ''MONTH''),
        hierarchy_label,
        hierarchy_id,
        hierarchy_level,
        case when metric_level = 1 then ''Total'' 
                else metric_category
                end, 
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                else metric_group 
                end,
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                when metric_level = 3 then metric_group
                else metric_label 
                end,
        case when metric_level = 1 then 0 
                when metric_level = 2 then 0
                when metric_level = 3 then 0
                else metric_id 
                end,
        case when metric_level = 1 then ''Total'' 
                when metric_level = 2 then metric_category
                when metric_level = 3 then metric_group
                else metric_modality 
                end,

        case when metric_level < 4 then null else METRIC_GOAL_LOW end,
        case when metric_level < 4 then null else METRIC_GOAL_MED end,
        case when metric_level < 4 then null else METRIC_GOAL_HIGH end,
        case when metric_level < 3 then null else METRIC_SCALE end,
        case when metric_level < 3 then null else METRIC_UP_MEANING end,
        metric_level
         ';
        
        BEGIN
        
        OPEN cur FOR sqlrequest;
        LOOP
            FETCH cur INTO
                post_date,
                hierarchy_label,
                hierarchy_id,
          	hierarchy_level,
                metric_total,
                metric_category, 
                metric_group,
                metric_label,
                metric_id,
                metric_modality,
                METRIC_GOAL_LOW,
                METRIC_GOAL_MED,
                METRIC_GOAL_HIGH,
                METRIC_SCALE,
                METRIC_UP_MEANING,
                metric_numerator, 
                metric_denominator, 
                metric_value,
                ultrascore,
                standardized_score,
                possible_score,
                cqs_score,
                cqs_flag,
                us_flag
             ;
            EXIT WHEN cur% notfound ;
            PIPE ROW(NEW metricDataObject(
                post_date,
                hierarchy_label,
                hierarchy_id,
          	hierarchy_level,
                metric_total,
                metric_category, 
                metric_group,
                metric_label,
                metric_id,
                metric_modality,
                METRIC_GOAL_LOW,
                METRIC_GOAL_MED,
                METRIC_GOAL_HIGH,
                METRIC_SCALE,
                METRIC_UP_MEANING,
                metric_numerator, 
                metric_denominator, 
                metric_value,
                ultrascore,
                standardized_score,
                possible_score,
                cqs_score,
                cqs_flag,
                us_flag
            ));
        END LOOP ;
        RETURN;
    END ;
/

/*
        p_filter_global_id VARCHAR2, - 'facility_id (padded with zeros to make 6 characters), or global_id of area, region, group and division'
        p_filter_hierarchy_level INT, - '0=facility, 1=area, 2=region, 3=group, 4=division'
        p_hierarchy_name VARCHAR2, 'organizational, ESCO'
        p_start_date VARCHAR2, 'YYYY-MM-DD format'
        p_end_date VARCHAR2, 'YYYY-MM-DD format'
        p_grouping_hierarchy_level INT, - '0=facility, 1=area, 2=region, 3=group, 4=division'
        p_modality VARCHAR2, - 'IHD, HHD, PD'
        p_grouping_metric_level INT - '1=Total, 2=category, 3=group, 4=metric/modality'
*/


select metric_id, metric_label
from metric_data_dim
where cqs_flag = 1
ORDER BY metric_label

select t.*, DENSE_RANK() OVER (ORDER BY metric_value) as rank
from (TABLE(get_metricDataObjects( '100005', 4, 'organizational', '2016-12-01', '2016-12-31', 1, '''IHD'',''PD''', 4 ))) t
where metric_id = 413

select *
from TABLE(get_metricDataObjects('008659', 0, 'organizational', '2016-12-01', '2016-12-31', 0, '''IHD''', 4, '
{
  "rows": [
    {
      "id": "310",
      "name": "Albumin IHD",
      "cqs_flag":0,
      "current": [
        24.64,
        34.82,
        45
      ],
      "new": [
        30.85153846153846,
        45.93538461538461,
        62.980769230769226
      ]
    },
    {
      "id": "331",
      "name": "Foot Check IHD",
      "current": [
        50,
        75,
        100
      ],
      "new": [
        69.50470219435736,
        87.47021943573668,
        100
      ]
    }
  ]
}
'))

select * 
from (
select post_date, hierarchy_label, hierarchy_label, metric_value, DENSE_RANK() OVER (ORDER BY metric_value) as rank -- metric_value
from TABLE(get_metricDataObjects('107696,100005,106917,110893,106943', 4, 'organizational', '2016-12-01','2016-12-31',3, '''IHD'',''HHD'',''PD''', 4)) tf
where metric_label = 'spKt/V 1.4 IHD'
) tf1
where rank <= 10


                select * 
                from FAC_DIAL_HIERARCHY f 
                where f."level" = ''facility''
                and hierlevel(f.bit_id, ' || p_filter_hierarchy_level || ', 6) in (' || p_filter_global_id || ')
                and hierarchy = ''' || p_hierarchy_name || '''

