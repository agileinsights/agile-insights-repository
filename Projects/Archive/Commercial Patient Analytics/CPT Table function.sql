-- define an object type

-- drop TYPE cohortDataObject_ntt;

create or replace TYPE BI_SRC.cohortDataObject AS OBJECT (

        rpt_dt DATE, 
        groupby1 VARCHAR2(225), 
        groupby2 VARCHAR2(225), 
        metric_cd VARCHAR2(225), 
        study_numerator number(18,6),
        study_denominator number(18,6),
        compare_numerator number(18,6),
        compare_denominator number(18,6)            

   );

-- define type "table" of MyObjects
create TYPE BI_SRC.cohortDataObject_ntt IS TABLE OF BI_SRC.cohortDataObject ;

--/
    create or replace FUNCTION BI_SRC.get_cohortData(
        p_fac_ids VARCHAR2,
        p_start_date VARCHAR2,
        p_end_date VARCHAR2,
        p_groupby1  VARCHAR2,
        p_groupby2  VARCHAR2,
        p_cohort VARCHAR2
    
    ) RETURN cohortDataObject_ntt 
    PIPELINED
    PARALLEL_ENABLE
    IS
        cur sys_refcursor ;

        rpt_dt DATE; 
        groupby1 VARCHAR2(225); 
        groupby2 VARCHAR2(225); 
        metric_cd VARCHAR2(225); 
        study_numerator number(18,6);
        study_denominator number(18,6);
        compare_numerator number(18,6);
        compare_denominator number(18,6);            
	
        sqlrequest varchar2(20000) := '

with patient_detail as (

        select
         PT_DIAL_ID,
         FAC_DIAL_ID,
         RPT_DT,
         ADT_ADM_RSN_NM,  
         ADT_DISCH_RSN_NM, 
         IS_PT_DX_ACUTE_IND,
         MOD_MODALITY_DESC, 
         MOD_SUBMODALITY_DESC,
         CASE WHEN COML_TENURE_DAYS_CNT < 120 THEN ''0-3 Mo''
              WHEN COML_TENURE_DAYS_CNT <= 209 THEN ''4-6 Mo''
              WHEN COML_TENURE_DAYS_CNT <= 389 THEN ''7-12 Mo''
              WHEN COML_TENURE_DAYS_CNT <= 569 THEN ''13-18 Mo''
              WHEN COML_TENURE_DAYS_CNT <= 1019 THEN ''19-33 Mo''
              ELSE ''> 33 Mo'' 
         END AS TENURE_BUCKET,
         PT_OUTCOME_TXT,
         POST_COB_IND,
         TYP_INS_LOSS_NM,
         RPT_DT_DATE_ID,
         PRIM_INS_HLTH_PLN_ID,
         PLCMNT_TYP_CD, 
         PLCMNT_TYP_NM,
         PLCMNT_MODIFIER_CD, 
         PLCMNT_MODIFIER_NM,
         NC_CD,
         PRAC_GRP_NM,
         TX_SETTING_NM,
         COB_PER_MTH_CNT,
         DISCH_DISP_NM,
         COML_STS_NM,
        
         COML_TENURE_MTH_CNT,
         CASE WHEN COML_STS_NM = ''DROP'' THEN 1 ELSE 0 END as DROPPED_PTS,
         CASE WHEN COML_STS_NM = ''ACTIVE'' THEN 1 ELSE 0 END as ACTIVE_PTS,
         COML_AMT_TOT_CHG + COML_AMT_GUAR_ADJ + COML_AMT_PYR_ADJ as COMPL_AMT,
         COML_TX_CNT,

         PTRAK_COB_REMAIN_MTH_CNT,
         PTRAK_COB_REMAIN_RATE_CNT,
         case when PTRAK_COB_REMAIN_RATE_CNT=1 then 1 else 0 end as PTRAK_COB_FLAG,
         CASE WHEN PTRAK_COB_REMAIN_RATE_CNT = 1 THEN PTRAK_COB_REMAIN_MTH_CNT ELSE 0 END as PTRAK_COB_REMAIN_FLAG,
         CASE WHEN PTRAK_COB_REMAIN_RATE_CNT = 1 THEN COB_PER_MTH_CNT ELSE 0 END as PTRAK_COB_MTH_CNT,

         PTRAK_FDOD_REMAIN_MTH_CNT,
         PTRAK_FDOD_REMAIN_RATE_CNT,
         CASE WHEN PTRAK_FDOD_REMAIN_RATE_CNT = 1 then 1 else 0 end as PTRAK_FDOD_FLAG,
         CASE WHEN PTRAK_FDOD_REMAIN_RATE_CNT = 1 THEN PTRAK_FDOD_REMAIN_MTH_CNT ELSE 0 END as PTRAK_FDOD_REMAIN_FLAG,
         CASE WHEN PTRAK_FDOD_REMAIN_RATE_CNT = 1 THEN COB_PER_MTH_CNT ELSE 0 END as  PTRAK_FDOD_MTH_CNT

         FROM BI_RPT.ZBI_PT_COML_INS_AGG c
         where c.FAC_DIAL_ID in 
                (select o.fac_dial_id from ORG.FAC_DIAL o where o.org_' || p_cohort || '_id in 
                        (select f.org_' || p_cohort || '_id from ORG.FAC_DIAL f where f.fac_dial_id in (' || p_fac_ids || '))
                )
        and rpt_dt >= trunc(to_date(''' || p_start_date || ''', ''YYYY-MM-DD''), ''MONTH'')
        and rpt_dt < trunc(to_date(''' || p_end_date || ''', ''YYYY-MM-DD'') + 1, ''MONTH'')
),
 
facility_pivot as ( 
 select *
  from (
         select 
                 fac_dial_id,
                 rpt_dt,
                 COML_STS_NM,
                 ' || p_groupby1 || ' as groupby1,
                 ' || p_groupby2 || ' as groupby2,
                 count(PT_DIAL_ID) as patient_count,
                 sum(COML_TENURE_MTH_CNT) as COML_TENURE_MTH_CNT,
                 sum(DROPPED_PTS) as DROPPED_PTS,
                 sum(ACTIVE_PTS) as ACTIVE_PTS,
                 sum(COMPL_AMT) as COMPL_AMT,
                 sum(COML_TX_CNT) as COML_TX_CNT,
                 sum(PTRAK_COB_REMAIN_MTH_CNT) as PTRAK_COB_REMAIN_MTH_CNT,
                 sum(PTRAK_COB_REMAIN_RATE_CNT) as PTRAK_COB_REMAIN_RATE_CNT,
                 sum(PTRAK_COB_REMAIN_FLAG) as PTRAK_COB_REMAIN_FLAG,
                 sum(PTRAK_COB_FLAG) as PTRAK_COB_FLAG,
                 sum(PTRAK_COB_MTH_CNT) as PTRAK_COB_MTH_CNT,
                 sum(PTRAK_FDOD_REMAIN_MTH_CNT) as PTRAK_FDOD_REMAIN_MTH_CNT,
                 sum(PTRAK_FDOD_REMAIN_RATE_CNT) as PTRAK_FDOD_REMAIN_RATE_CNT,
                 sum(PTRAK_FDOD_REMAIN_FLAG) as PTRAK_FDOD_REMAIN_FLAG,
                 sum(PTRAK_FDOD_FLAG) as PTRAK_FDOD_FLAG,
                 sum(PTRAK_FDOD_MTH_CNT) as PTRAK_FDOD_MTH_CNT
        from patient_detail
        group by 
                 fac_dial_id,
                 rpt_dt,
                 COML_STS_NM,
                 ' || p_groupby1 || ',
                 ' || p_groupby2 || ' 
) denormal 
unpivot
(
  metric_value
    for metric_code in (
                 patient_count,
                 "COML_TENURE_MTH_CNT",
                 "DROPPED_PTS",
                 "ACTIVE_PTS",
                 "COMPL_AMT",
                 "COML_TX_CNT",
                 "PTRAK_COB_REMAIN_MTH_CNT",
                 "PTRAK_COB_REMAIN_RATE_CNT",
                 "PTRAK_COB_REMAIN_FLAG",
                 "PTRAK_COB_FLAG",
                 "PTRAK_COB_MTH_CNT",
                 "PTRAK_FDOD_REMAIN_MTH_CNT",
                 "PTRAK_FDOD_REMAIN_RATE_CNT",
                 "PTRAK_FDOD_REMAIN_FLAG",
                 "PTRAK_FDOD_FLAG",
                 "PTRAK_FDOD_MTH_CNT"
    )
)
),

metric_data_dim as (

        select 1 as METRIC_ID,''ACTIVE_Tenure'' as metric_cd, ''ACTIVE_COML_TENURE_MTH_CNT'' AS NUMERATOR, ''ACTIVE_patient_count'' AS DENOMINATOR, 1 as sortorder,''operational'' as metricgroup,1 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''good'' as incrMean,''Active Avg FKC Tenure'' as metricabbrv, ''Active Avg FKC Tenure'' as metriclabel, ''rate'' as calc_type from dual
        union select 3 as METRIC_ID,''ACTIVE_Count'' as metric_cd, ''ACTIVE_patient_count'' AS NUMERATOR, ''1'' AS DENOMINATOR,2 as sortorder,''operational'' as metricgroup,0 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''good'' as incrMean,''Active Pt Cnt'' as metricabbrv, ''Active Patient Count'' as metriclabel, ''count'' as calc_type from dual
        union select 2 as METRIC_ID,''DROP_Tenure'' as metric_cd, ''DROP_COML_TENURE_MTH_CNT'' AS NUMERATOR, ''DROP_patient_count'' AS DENOMINATOR,7 as sortorder,''operational'' as metricgroup,1 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''good'' as incrMean,''Drop Avg FKC Tenure'' as metricabbrv, ''Drop Avg FKC Tenure'' as metriclabel, ''rate'' as calc_type from dual
        union select 4 as METRIC_ID,''DROP_Count'' as metric_cd, ''DROP_patient_count'' AS NUMERATOR, ''1'' AS DENOMINATOR,8 as sortorder,''operational'' as metricgroup,0 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''bad'' as incrMean,''Drop Pt Cnt'' as metricabbrv, ''Drop Patient Count'' as metriclabel, ''count'' as calc_type from dual
        union select 6 as METRIC_ID,''ACTIVE_RPT'' as metric_cd, ''ACTIVE_COMPL_AMT'' AS NUMERATOR, ''ACTIVE_COML_TX_CNT'' AS DENOMINATOR,12 as sortorder,''operational'' as metricgroup,2 as "decimal",1 as scale,''$'' as symbol,'''' as suffix,''good'' as incrMean,''Active Rev/Tmt'' as metricabbrv, ''Active Rev Per Treatment'' as metriclabel, ''rate'' as calc_type from dual
        union select 7 as METRIC_ID,''DROP_RPT'' as metric_cd, ''DROP_COMPL_AMT'' AS NUMERATOR, ''DROP_COML_TX_CNT'' AS DENOMINATOR,13 as sortorder,''operational'' as metricgroup,2 as "decimal",1 as scale,''$'' as symbol,'''' as suffix,''bad'' as incrMean,''Drop Rev/Tmt'' as metricabbrv, ''Drop Rev Per Treatment'' as metriclabel, ''rate'' as calc_type from dual
        union select 8 as METRIC_ID,''ACTIVE_Mth_Rem_PTCOB'' as metric_cd, ''ACTIVE_PTRAK_COB_REMAIN_MTH_CNT'' AS NUMERATOR, ''ACTIVE_PTRAK_COB_REMAIN_RATE_CNT'' AS DENOMINATOR,14 as sortorder,''operational'' as metricgroup,1 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''good'' as incrMean,''Active Elig Period (PT COB) Months Remaining'' as metricabbrv,''Active Elig Period (PT COB) Months Remaining'' as metriclabel, ''rate'' as calc_type from dual
        union select 9 as METRIC_ID,''DROP_Mth_Rem_PTCOB'' as metric_cd, ''DROP_PTRAK_COB_FLAG'' NUMERATOR, ''1'' AS DENOMINATOR,15 as sortorder,''operational'' as metricgroup,1 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''bad'' as incrMean,''Drop Elig Period (PT COB) Months Remaining'' as metricabbrv,''Drop Elig Period (PT COB) Months Remaining'' as metriclabel, ''rate'' as calc_type from dual
        union select 10 as METRIC_ID,''ACTIVE_Count_PTCOB'' as metric_cd, ''ACTIVE_PTRAK_FDOD_FLAG'' AS NUMERATOR, ''1'' AS DENOMINATOR,18 as sortorder,''operational'' as metricgroup,0 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''good'' as incrMean,''Active Elig Period (PT COB) Pt Cnt'' as metricabbrv,''Active Eligibility Period (PT COB) Patient Count'' as metriclabel, ''count'' as calc_type from dual
        union select 11 as METRIC_ID,''DROP_Count_PTCOB'' as metric_cd, ''DROP_PTRAK_COB_FLAG'' AS NUMERATOR, ''1'' AS DENOMINATOR,19 as sortorder,''operational'' as metricgroup,0 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''bad'' as incrMean,''Drop Elig Period (PT COB) Pt Cnt'' as metricabbrv,''Drop Eligibility Period (PT COB) Patient Count'' as metriclabel, ''count'' as calc_type from dual
        union select 12 as METRIC_ID,''ACTIVE_Mth_Rem_PTFDOD'' as metric_cd, ''ACTIVE_PTRAK_FDOD_REMAIN_FLAG'' AS NUMERATOR, ''ACTIVE_PTRAK_COB_MTH_CNT'' AS DENOMINATOR,20 as sortorder,''operational'' as metricgroup,1 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''good'' as incrMean,''Active Elig Period (PT FDOD) Months Remaining'' as metricabbrv,''Active Eligibility Period (PT FDOD) Months Remaining'' as metriclabel, ''rate'' as calc_type from dual
        union select 13 as METRIC_ID,''DROP_Mth_Rem_PTFDOD'' as metric_cd, ''DROP_PTRAK_FDOD_REMAIN_MTH_CNT'' AS NUMERATOR, ''DROP_PTRAK_FDOD_REMAIN_RATE_CNT'' AS DENOMINATOR,21 as sortorder,''operational'' as metricgroup,1 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''bad'' as incrMean,''Drop Elig Period (PT FDOD) Months Remaining'' as metricabbrv,''Drop Eligibility Period (PT FDOD) Months Remaining'' as metriclabel, ''rate'' as calc_type from dual
        union select 14 as METRIC_ID,''ACTIVE_Count_PTFDOD'' as metric_cd, ''ACTIVE_PTRAK_FDOD_FLAG'' AS NUMERATOR, ''1'' AS DENOMINATOR,22 as sortorder,''operational'' as metricgroup,0 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''good'' as incrMean,''Active Elig Period (PT FDOD) Pt Cnt'' as metricabbrv,''Active Eligibility Period (PT FDoD) Patient Count'' as metriclabel, ''count'' as calc_type from dual
        union select 15 as METRIC_ID,''DROP_Count_PTFDOD'' as metric_cd,  ''DROP_PTRAK_FDOD_FLAG'' AS NUMERATOR, ''1'' AS DENOMINATOR,23 as sortorder,''operational'' as metricgroup,0 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''bad'' as incrMean,''Drop Elig Period (PT FDOD) Pt Cnt'' as metricabbrv,''Drop Eligibility Period (PT FDoD) Patient Count'' as metriclabel, ''count'' as calc_type from dual
        union select 16 as METRIC_ID,''ACTIVE_ER_REM_PTCOB'' as metric_cd, ''ACTIVE_PTRAK_FDOD_REMAIN_FLAG'' AS NUMERATOR, ''ACTIVE_PTRAK_COB_MTH_CNT'' AS DENOMINATOR,24 as sortorder,''operational'' as metricgroup,1 as "decimal",100 as scale,'''' as symbol,''%'' as suffix,''good'' as incrMean,''Active Elig Period (PT COB) % Remaining'' as metricabbrv,''Active Eligibility Period (PT COB) % Remaining'' as metriclabel, ''rate'' as calc_type from dual
        union select 17 as METRIC_ID,''DROP_ER_REM_PTCOB'' as metric_cd,  ''DROP_PTRAK_FDOD_REMAIN_FLAG'' AS NUMERATOR, ''DROP_PTRAK_COB_MTH_CNT'' AS DENOMINATOR,25 as sortorder,''operational'' as metricgroup,1 as "decimal",100 as scale,'''' as symbol,''%'' as suffix,''bad'' as incrMean,''Drop Elig Period (PT COB) % Remaining'' as metricabbrv,''Drop Eligibility Period (PT COB) % Remaining'' as metriclabel, ''rate'' as calc_type from dual
        union select 18 as METRIC_ID,''ACTIVE_ER_REM_PTFDOD'' as metric_cd, ''ACTIVE_PTRAK_FDOD_REMAIN_FLAG'' AS NUMERATOR, ''ACTIVE_PTRAK_FDOD_MTH_CNT'' AS DENOMINATOR,26 as sortorder,''operational'' as metricgroup,1 as "decimal",100 as scale,'''' as symbol,''%'' as suffix,''good'' as incrMean,''Active Elig Period (PT FDOD) % Remaining'' as metricabbrv,''Active Eligibility Period (PT FDoD) % Remaining'' as metriclabel, ''rate'' as calc_type from dual
        union select 19 as METRIC_ID,''DROP_ER_REM_PTFDOD'' as metric_cd, ''DROP_PTRAK_FDOD_REMAIN_FLAG'' AS NUMERATOR, ''DROP_PTRAK_FDOD_MTH_CNT'' AS DENOMINATOR,27 as sortorder,''operational'' as metricgroup,1 as "decimal",100 as scale,'''' as symbol,''%'' as suffix,''bad'' as incrMean,''Drop Elig Period (PT FDOD) % Remaining'' as metricabbrv,''Drop Eligibility Period (PT FDoD) % Remaining'' as metriclabel, ''rate'' as calc_type from dual
        union select 20 as METRIC_ID,''DROP_Mth_Rem_PTCOB_3M_AVG'' as metric_cd, ''DROP_PTRAK_COB_REMAIN_MTH_CNT'' AS NUMERATOR, ''DROP_PTRAK_COB_REMAIN_RATE_CNT'' AS DENOMINATOR,28 as sortorder,''operational'' as metricgroup,1 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''bad'' as incrMean,''Drop Elig Period (PT COB) Months Remaining 3-month Avg'' as metricabbrv,''Drop Elig Period (PT COB) Months Remaining 3-month Avg'' as metriclabel, ''rate'' as calc_type from dual
        union select 21 as METRIC_ID,''DROP_PT_CNT_3M_AVG'' as metric_cd, ''DROP_patient_count'' AS NUMERATOR, ''1'' AS DENOMINATOR,29 as sortorder,''operational'' as metricgroup,0 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''bad'' as incrMean,''Drop Pt Cnt 3-month Avg'' as metricabbrv, ''Drop Patient Count 3-month Avg'' as metriclabel, ''count'' as calc_type from dual
        union select 22 as METRIC_ID,''DROP_Tenure_3M_AVG'' as metric_cd, ''DROP_COML_TENURE_MTH_CNT'' AS NUMERATOR,  ''DROP_patient_count'' AS DENOMINATOR, 30 as sortorder,''operational'' as metricgroup,1 as "decimal",1 as scale,'''' as symbol,'''' as suffix,''good'' as incrMean,''Drop Avg FKC Tenure 3-month Avg'' as metricabbrv, ''Drop Avg FKC Tenure 3-month Avg'' as metriclabel, ''rate'' as calc_type from dual
),

facility_detail as (

        select fac_dial_id, rpt_dt, groupby1, groupby2, coml_sts_nm || ''_'' || metric_code as metric_cd, metric_value
        from facility_pivot
), 

metric_data_dim_pivot as (

        select metric_id, metric_cd, calc_type as metric_calc, numerator as METRIC_part, ''numerator'' as metric_position
        FROM METRIC_DATA_DIM 
        union
        select metric_id, metric_cd, calc_type as metric_calc, denominator as METRIC_part, ''denominator'' as metric_position
        FROM METRIC_DATA_DIM 
),

fac_cohort as (
        select fac_dial_id, ''study'' as cohort
        from org.fac_dial
        where fac_dial_id in (' || p_fac_ids || ') 
        
        union
        select fac_dial_id, ''compare'' as cohort
        from org.fac_dial
        where org_' || p_cohort || '_id in 
                (select f.org_' || p_cohort || '_id from ORG.FAC_DIAL f where f.fac_dial_id in (' || p_fac_ids || '))
        and fac_dial_id not in (' || p_fac_ids || ') 
                )

select f.rpt_dt, f.groupby1, f.groupby2, m.metric_cd, 
        sum(case when m.metric_position = ''numerator'' and c.cohort = ''study'' then nvl(f.metric_value,0) else 0 end) as study_numerator,
        sum(case when m.metric_position = ''denominator'' and c.cohort = ''study'' then nvl(f.metric_value,0) else 0 end) as study_denominator,
        sum(case when m.metric_position = ''numerator'' and c.cohort = ''compare'' then nvl(f.metric_value,0) else 0 end) as compare_numerator,
        sum(case when m.metric_position = ''denominator'' and c.cohort = ''compare'' then nvl(f.metric_value,0) else 0 end) as compare_denominator
from fac_cohort c inner join 
        facility_detail f on c.fac_dial_id = f.fac_dial_id inner join
        metric_data_dim_pivot m on f.metric_cd = m.metric_part
group by f.rpt_dt, f.groupby1, f.groupby2, m.metric_cd

         ';
        
        BEGIN
        
        OPEN cur FOR sqlrequest;
        LOOP
            FETCH cur INTO
            
                rpt_dt, 
                groupby1, 
                groupby2, 
                metric_cd, 
                study_numerator,
                study_denominator,
                compare_numerator,
                compare_denominator            
            
             ;
            EXIT WHEN cur% notfound ;
            PIPE ROW(NEW cohortDataObject(

                rpt_dt, 
                groupby1, 
                groupby2, 
                metric_cd, 
                study_numerator,
                study_denominator,
                compare_numerator,
                compare_denominator            

            ));
        END LOOP ;
        RETURN;
    END ;
/

/*
        'p_fac_ids',
        'p_start_date',
        'p_end_date',
        'p_groupby1',
        'p_groupby2',
        'p_cohort'
*/



select * 
from TABLE(get_cohortData(
        '2490, 2481',
        '2016-01-01',
        '2017-01-01',
        'post_cob_ind',
        'typ_ins_loss_nm',
        'area'
))


  select * 
  from TABLE(get_cohortData(
          '2490, 2481',
          '2016-01-01',
          '2017-01-01',
          'ADT_ADM_RSN_NM',
          'ADT_ADM_RSN_NM',
          'regn'
  ))