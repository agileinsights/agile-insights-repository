Role	Employee ID	BD Group	BD Region	DHPS Region	HPS Territory
National	2048219	Alaska	Alaska	DHPSM Alaska 01 (Open)	Alaska
National	2048219	Capitol Lakes	Capitol	DHPSM William Warner	Baltimore
National	2048219	Capitol Lakes	Capitol	DHPSM William Warner	Washington DC
National	2048219	Capitol Lakes	East Virginia	DHPSM William Warner	Washington DC
National	2048219	Capitol Lakes	East Virginia	DHPSM William Warner	Richmond
National	2048219	Capitol Lakes	East Virginia	DHPSM William Warner	Roanoke
National	2048219	Capitol Lakes	Michigan	DHPSM Lisa Reedy	Ann Arbor
National	2048219	Capitol Lakes	Michigan	DHPSM Lisa Reedy	Detroit
National	2048219	Capitol Lakes	Michigan	DHPSM Lisa Reedy	Kalamazoo
National	2048219	Capitol Lakes	Ohio	DHPSM Lisa Reedy	Kalamazoo
National	2048219	Capitol Lakes	Ohio	DHPSM Lisa Reedy	Canton
National	2048219	Capitol Lakes	Ohio	DHPSM Lisa Reedy	Cleveland
National	2048219	Capitol Lakes	Western OH	DHPSM William Warner	Cincinnati
National	2048219	Capitol Lakes	Western OH	DHPSM William Warner	Columbus
National	2048219	Capitol Lakes	Western OH	DHPSM Lisa Reedy	Fort Wayne
National	2048219	Capitol Lakes	Western PA	DHPSM William Warner	Baltimore
National	2048219	Capitol Lakes	Western PA	DHPSM William Warner	Pittsburgh
National	2048219	Capitol Lakes	Western PA	DHPSM Lisa Reedy	Canton
National	2048219	Great Plains	Austin-New Mexico	DHPSM Ashley Lemaster	Albuquerque
National	2048219	Great Plains	Austin-New Mexico	DHPSM Ashley Lemaster	Austin
National	2048219	Great Plains	Kansas City	DHPSM Kristen Oyler	Kansas City East
National	2048219	Great Plains	Kansas City	DHPSM Kristen Oyler	Kansas City West
National	2048219	Great Plains	Kansas City	DHPSM Kristen Oyler	Omaha
National	2048219	Great Plains	North Texas	DHPSM Ashley Lemaster	Dallas
National	2048219	Great Plains	North Texas	DHPSM Ashley Lemaster	Fort Worth
National	2048219	Great Plains	Rocky Mountain	DHPSM Kristen Oyler	Denver
National	2048219	Great Plains	Rocky Mountain	DHPSM Kristen Oyler	Salt Lake City
National	2048219	Great Plains	Rocky Mountain	DHPSM Kristen Oyler	Spokane
National	2048219	Great Plains	West Texas/Oklahoma	DHPSM Ashley Lemaster	Lubbock
National	2048219	Great Plains	West Texas/Oklahoma	DHPSM Ashley Lemaster	Oklahoma City
National	2048219	Gulf Coast	Houston	DHPSM Cynthia Brown	Central Houston
National	2048219	Gulf Coast	Houston	DHPSM Cynthia Brown	North Houston
National	2048219	Gulf Coast	Houston	DHPSM Cynthia Brown	South Houston
National	2048219	Gulf Coast	Mid-South	DHPSM Dennis Duthu	Memphis
National	2048219	Gulf Coast	Miss/NELA	DHPSM Dennis Duthu	Jackson
National	2048219	Gulf Coast	Miss/NELA	DHPSM Dennis Duthu	North New Orleans
National	2048219	Gulf Coast	Miss/SELA	DHPSM Dennis Duthu	Baton Rouge
National	2048219	Gulf Coast	Miss/SELA	DHPSM Dennis Duthu	South New Orleans
National	2048219	Gulf Coast	South Texas	DHPSM Cynthia Brown	North Houston
National	2048219	Gulf Coast	South Texas	DHPSM Cynthia Brown	North San Antonio
National	2048219	Gulf Coast	South Texas	DHPSM Cynthia Brown	South San Antonio
National	2048219	Gulf Coast	West LA/ETEX	DHPSM Cynthia Brown	Shreveport
National	2048219	Gulf Coast	West LA/ETEX	DHPSM Dennis Duthu	LafayetteLA
National	2048219	Mid-Atlantic	Appalachian Mountain	DHPSM David Gandy	Charleston
National	2048219	Mid-Atlantic	Eastern Tennessee	DHPSM David Gandy	Knoxville East
National	2048219	Mid-Atlantic	Eastern Tennessee	DHPSM David Gandy	Knoxville West
National	2048219	Mid-Atlantic	Eastern Tennessee	DHPSM David Gandy	Nashville
National	2048219	Mid-Atlantic	Kentucky	DHPSM David Gandy	Knoxville West
National	2048219	Mid-Atlantic	Kentucky	DHPSM David Gandy	Lexington
National	2048219	Mid-Atlantic	Kentucky	DHPSM David Gandy	Louisville
National	2048219	Mid-Atlantic	North Carolina	DHPSM David Gandy	Charlotte
National	2048219	Mid-Atlantic	North Carolina	DHPSM David Gandy	Greensboro
National	2048219	Mid-Atlantic	North Carolina	DHPSM David Gandy	Raleigh
National	2048219	Mid-Atlantic	Tennessee Valley	DHPSM David Gandy	Nashville
National	2048219	Mid-Atlantic	Tennessee Valley	DHPSM David Gandy	Birmingham
National	2048219	Midwest	Chicago/Indiana	DHPSM Cynthia Black	Northern Indianapolis
National	2048219	Midwest	Chicago/Indiana	DHPSM Cynthia Black	Southern Indianapolis
National	2048219	Midwest	Chicago/Indiana	DHPSM Kevin Fontana	Chicago01
National	2048219	Midwest	Chicago/Indiana	DHPSM Kevin Fontana	Chicago02
National	2048219	Midwest	INIM/NIM	DHPSM Cynthia Black	Northern Indianapolis
National	2048219	Midwest	INIM/NIM	DHPSM Cynthia Black	Southern Indianapolis
National	2048219	Midwest	St. Louis	DHPSM Cynthia Black	St. Louis East
National	2048219	Midwest	St. Louis	DHPSM Cynthia Black	St. Louis West
National	2048219	Midwest	Upper Midwest	DHPSM Kevin Fontana	Milwaukee
National	2048219	Midwest	Upper Midwest	DHPSM Kevin Fontana	Minneapolis
National	2048219	Midwest	West Chicago	DHPSM Cynthia Black	Peoria
National	2048219	Midwest	West Chicago	DHPSM Kevin Fontana	Chicago01
National	2048219	Midwest	West Chicago	DHPSM Kevin Fontana	Chicago02
National	2048219	Midwest	West Chicago	DHPSM Kevin Fontana	W. Chicago01
National	2048219	Midwest	West Chicago	DHPSM Kevin Fontana	W. Chicago02
National	2048219	Northeast	Eastern PA/Delaware	DHPSM Annmarie Kouser	Central Pennsylvania
National	2048219	Northeast	Eastern PA/Delaware	DHPSM Annmarie Kouser	Delaware
National	2048219	Northeast	Eastern PA/Delaware	DHPSM Annmarie Kouser	Eastern Pennsylvania
National	2048219	Northeast	Eastern PA/Delaware	DHPSM Annmarie Kouser	South Jersey
National	2048219	Northeast	Metro New York	DHPSM Annmarie Kouser	Newark
National	2048219	Northeast	Metro New York	DHPSM Nicole Barnett	Brooklyn/Long Island
National	2048219	Northeast	Metro New York	DHPSM Nicole Barnett	Connecticut
National	2048219	Northeast	Metro New York	DHPSM Nicole Barnett	Manhattan
National	2048219	Northeast	New England	DHPSM Nicole Barnett	N New England
National	2048219	Northeast	New England	DHPSM Nicole Barnett	S New England
National	2048219	Northeast	Northeast PA	DHPSM Annmarie Kouser	Central Pennsylvania
National	2048219	Northeast	Northeast PA	DHPSM Annmarie Kouser	Newark
National	2048219	Northeast	Northeast PA	DHPSM Annmarie Kouser	Scranton
National	2048219	Northeast	Southern New Jersey	DHPSM Annmarie Kouser	Central Pennsylvania
National	2048219	Northeast	Southern New Jersey	DHPSM Annmarie Kouser	South Jersey
National	2048219	Northeast	Southern New Jersey	DHPSM Annmarie Kouser	Newark
National	2048219	Northeast	Southern New Jersey	DHPSM Annmarie Kouser	Toms River
National	2048219	Northeast	Western NY	DHPSM Nicole Barnett	N New England
National	2048219	Northeast	Western NY	DHPSM Nicole Barnett	Buffalo
National	2048219	Northeast	Western NY	DHPSM Nicole Barnett	Syracuse
National	2048219	Northeast	Western NY	DHPSM Nicole Barnett	W Massachusetts
National	2048219	Puerto Rico	Puerto Rico	DHPSM Puerto 01 (Open)	Puerto Rico
National	2048219	Southeast	North Florida	DHPSM Angela Jackson	Jacksonville
National	2048219	Southeast	North Florida	DHPSM Kathleen Murray	Fort Lauderdale
National	2048219	Southeast	North Florida	DHPSM Kathleen Murray	Orlando
National	2048219	Southeast	North Georgia	DHPSM Angela Jackson	Macon
National	2048219	Southeast	North Georgia	DHPSM Angela Jackson	Savannah
National	2048219	Southeast	North Georgia	DHPSM Kathleen Murray	Atlanta North
National	2048219	Southeast	North Georgia	DHPSM Kathleen Murray	Atlanta West
National	2048219	Southeast	South Alabama/Florida Panhandle	DHPSM Angela Jackson	Savannah
National	2048219	Southeast	South Alabama/Florida Panhandle	DHPSM Angela Jackson	Montgomery
National	2048219	Southeast	South Carolina	DHPSM Angela Jackson	Macon
National	2048219	Southeast	South Carolina	DHPSM Angela Jackson	Savannah
National	2048219	Southeast	South Carolina	DHPSM Angela Jackson	Columbia
National	2048219	Southeast	South Florida	DHPSM Kathleen Murray	Fort Lauderdale
National	2048219	Southeast	South Florida	DHPSM Kathleen Murray	Fort Myers
National	2048219	Southeast	South Florida	DHPSM Kathleen Murray	Miami
National	2048219	Western	Arizona	DHPSM Western 01 (Open)	Phoenix North
National	2048219	Western	Arizona	DHPSM Western 01 (Open)	Phoenix South
National	2048219	Western	Hawaii	DHPSM Hawaii 01 (Open)	Hawaii
National	2048219	Western	LA/Nevada	DHPSM Western 01 (Open)	Las Vegas/Fresno
National	2048219	Western	LA/Nevada	DHPSM Franklyn Harris	Los Angeles North
National	2048219	Western	LA/Nevada	DHPSM Franklyn Harris	Los Angeles South
National	2048219	Western	Northern California	DHPSM Western 01 (Open)	Las Vegas/Fresno
National	2048219	Western	Northern California	DHPSM Western 01 (Open)	Reno/Sacramento
National	2048219	Western	Northern California	DHPSM Western 01 (Open)	San Francisco
National	2048219	Western	Pacific Northwest	DHPSM Western 01 (Open)	Portland
National	2048219	Western	Pacific Northwest	DHPSM Western 01 (Open)	Seattle
National	2048219	Western	Southern California	DHPSM Franklyn Harris	San Bernardino
National	2048219	Western	Southern California	DHPSM Franklyn Harris	San Diego North
National	2048219	Western	Southern California	DHPSM Franklyn Harris	San Diego South
National	3173879	Alaska	Alaska	DHPSM Alaska 01 (Open)	Alaska
National	3173879	Capitol Lakes	Capitol	DHPSM William Warner	Baltimore
National	3173879	Capitol Lakes	Capitol	DHPSM William Warner	Washington DC
National	3173879	Capitol Lakes	East Virginia	DHPSM William Warner	Washington DC
National	3173879	Capitol Lakes	East Virginia	DHPSM William Warner	Richmond
National	3173879	Capitol Lakes	East Virginia	DHPSM William Warner	Roanoke
National	3173879	Capitol Lakes	Michigan	DHPSM Lisa Reedy	Ann Arbor
National	3173879	Capitol Lakes	Michigan	DHPSM Lisa Reedy	Detroit
National	3173879	Capitol Lakes	Michigan	DHPSM Lisa Reedy	Kalamazoo
National	3173879	Capitol Lakes	Ohio	DHPSM Lisa Reedy	Kalamazoo
National	3173879	Capitol Lakes	Ohio	DHPSM Lisa Reedy	Canton
National	3173879	Capitol Lakes	Ohio	DHPSM Lisa Reedy	Cleveland
National	3173879	Capitol Lakes	Western OH	DHPSM William Warner	Cincinnati
National	3173879	Capitol Lakes	Western OH	DHPSM William Warner	Columbus
National	3173879	Capitol Lakes	Western OH	DHPSM Lisa Reedy	Fort Wayne
National	3173879	Capitol Lakes	Western PA	DHPSM William Warner	Baltimore
National	3173879	Capitol Lakes	Western PA	DHPSM William Warner	Pittsburgh
National	3173879	Capitol Lakes	Western PA	DHPSM Lisa Reedy	Canton
National	3173879	Great Plains	Austin-New Mexico	DHPSM Ashley Lemaster	Albuquerque
National	3173879	Great Plains	Austin-New Mexico	DHPSM Ashley Lemaster	Austin
National	3173879	Great Plains	Kansas City	DHPSM Kristen Oyler	Kansas City East
National	3173879	Great Plains	Kansas City	DHPSM Kristen Oyler	Kansas City West
National	3173879	Great Plains	Kansas City	DHPSM Kristen Oyler	Omaha
National	3173879	Great Plains	North Texas	DHPSM Ashley Lemaster	Dallas
National	3173879	Great Plains	North Texas	DHPSM Ashley Lemaster	Fort Worth
National	3173879	Great Plains	Rocky Mountain	DHPSM Kristen Oyler	Denver
National	3173879	Great Plains	Rocky Mountain	DHPSM Kristen Oyler	Salt Lake City
National	3173879	Great Plains	Rocky Mountain	DHPSM Kristen Oyler	Spokane
National	3173879	Great Plains	West Texas/Oklahoma	DHPSM Ashley Lemaster	Lubbock
National	3173879	Great Plains	West Texas/Oklahoma	DHPSM Ashley Lemaster	Oklahoma City
National	3173879	Gulf Coast	Houston	DHPSM Cynthia Brown	Central Houston
National	3173879	Gulf Coast	Houston	DHPSM Cynthia Brown	North Houston
National	3173879	Gulf Coast	Houston	DHPSM Cynthia Brown	South Houston
National	3173879	Gulf Coast	Mid-South	DHPSM Dennis Duthu	Memphis
National	3173879	Gulf Coast	Miss/NELA	DHPSM Dennis Duthu	Jackson
National	3173879	Gulf Coast	Miss/NELA	DHPSM Dennis Duthu	North New Orleans
National	3173879	Gulf Coast	Miss/SELA	DHPSM Dennis Duthu	Baton Rouge
National	3173879	Gulf Coast	Miss/SELA	DHPSM Dennis Duthu	South New Orleans
National	3173879	Gulf Coast	South Texas	DHPSM Cynthia Brown	North Houston
National	3173879	Gulf Coast	South Texas	DHPSM Cynthia Brown	North San Antonio
National	3173879	Gulf Coast	South Texas	DHPSM Cynthia Brown	South San Antonio
National	3173879	Gulf Coast	West LA/ETEX	DHPSM Cynthia Brown	Shreveport
National	3173879	Gulf Coast	West LA/ETEX	DHPSM Dennis Duthu	LafayetteLA
National	3173879	Mid-Atlantic	Appalachian Mountain	DHPSM David Gandy	Charleston
National	3173879	Mid-Atlantic	Eastern Tennessee	DHPSM David Gandy	Knoxville East
National	3173879	Mid-Atlantic	Eastern Tennessee	DHPSM David Gandy	Knoxville West
National	3173879	Mid-Atlantic	Eastern Tennessee	DHPSM David Gandy	Nashville
National	3173879	Mid-Atlantic	Kentucky	DHPSM David Gandy	Knoxville West
National	3173879	Mid-Atlantic	Kentucky	DHPSM David Gandy	Lexington
National	3173879	Mid-Atlantic	Kentucky	DHPSM David Gandy	Louisville
National	3173879	Mid-Atlantic	North Carolina	DHPSM David Gandy	Charlotte
National	3173879	Mid-Atlantic	North Carolina	DHPSM David Gandy	Greensboro
National	3173879	Mid-Atlantic	North Carolina	DHPSM David Gandy	Raleigh
National	3173879	Mid-Atlantic	Tennessee Valley	DHPSM David Gandy	Nashville
National	3173879	Mid-Atlantic	Tennessee Valley	DHPSM David Gandy	Birmingham
National	3173879	Midwest	Chicago/Indiana	DHPSM Cynthia Black	Northern Indianapolis
National	3173879	Midwest	Chicago/Indiana	DHPSM Cynthia Black	Southern Indianapolis
National	3173879	Midwest	Chicago/Indiana	DHPSM Kevin Fontana	Chicago01
National	3173879	Midwest	Chicago/Indiana	DHPSM Kevin Fontana	Chicago02
National	3173879	Midwest	INIM/NIM	DHPSM Cynthia Black	Northern Indianapolis
National	3173879	Midwest	INIM/NIM	DHPSM Cynthia Black	Southern Indianapolis
National	3173879	Midwest	St. Louis	DHPSM Cynthia Black	St. Louis East
National	3173879	Midwest	St. Louis	DHPSM Cynthia Black	St. Louis West
National	3173879	Midwest	Upper Midwest	DHPSM Kevin Fontana	Milwaukee
National	3173879	Midwest	Upper Midwest	DHPSM Kevin Fontana	Minneapolis
National	3173879	Midwest	West Chicago	DHPSM Cynthia Black	Peoria
National	3173879	Midwest	West Chicago	DHPSM Kevin Fontana	Chicago01
National	3173879	Midwest	West Chicago	DHPSM Kevin Fontana	Chicago02
National	3173879	Midwest	West Chicago	DHPSM Kevin Fontana	W. Chicago01
National	3173879	Midwest	West Chicago	DHPSM Kevin Fontana	W. Chicago02
National	3173879	Northeast	Eastern PA/Delaware	DHPSM Annmarie Kouser	Central Pennsylvania
National	3173879	Northeast	Eastern PA/Delaware	DHPSM Annmarie Kouser	Delaware
National	3173879	Northeast	Eastern PA/Delaware	DHPSM Annmarie Kouser	Eastern Pennsylvania
National	3173879	Northeast	Eastern PA/Delaware	DHPSM Annmarie Kouser	South Jersey
National	3173879	Northeast	Metro New York	DHPSM Annmarie Kouser	Newark
National	3173879	Northeast	Metro New York	DHPSM Nicole Barnett	Brooklyn/Long Island
National	3173879	Northeast	Metro New York	DHPSM Nicole Barnett	Connecticut
National	3173879	Northeast	Metro New York	DHPSM Nicole Barnett	Manhattan
National	3173879	Northeast	New England	DHPSM Nicole Barnett	N New England
National	3173879	Northeast	New England	DHPSM Nicole Barnett	S New England
National	3173879	Northeast	Northeast PA	DHPSM Annmarie Kouser	Central Pennsylvania
National	3173879	Northeast	Northeast PA	DHPSM Annmarie Kouser	Newark
National	3173879	Northeast	Northeast PA	DHPSM Annmarie Kouser	Scranton
National	3173879	Northeast	Southern New Jersey	DHPSM Annmarie Kouser	Central Pennsylvania
National	3173879	Northeast	Southern New Jersey	DHPSM Annmarie Kouser	South Jersey
National	3173879	Northeast	Southern New Jersey	DHPSM Annmarie Kouser	Newark
National	3173879	Northeast	Southern New Jersey	DHPSM Annmarie Kouser	Toms River
National	3173879	Northeast	Western NY	DHPSM Nicole Barnett	N New England
National	3173879	Northeast	Western NY	DHPSM Nicole Barnett	Buffalo
National	3173879	Northeast	Western NY	DHPSM Nicole Barnett	Syracuse
National	3173879	Northeast	Western NY	DHPSM Nicole Barnett	W Massachusetts
National	3173879	Puerto Rico	Puerto Rico	DHPSM Puerto 01 (Open)	Puerto Rico
National	3173879	Southeast	North Florida	DHPSM Angela Jackson	Jacksonville
National	3173879	Southeast	North Florida	DHPSM Kathleen Murray	Fort Lauderdale
National	3173879	Southeast	North Florida	DHPSM Kathleen Murray	Orlando
National	3173879	Southeast	North Georgia	DHPSM Angela Jackson	Macon
National	3173879	Southeast	North Georgia	DHPSM Angela Jackson	Savannah
National	3173879	Southeast	North Georgia	DHPSM Kathleen Murray	Atlanta North
National	3173879	Southeast	North Georgia	DHPSM Kathleen Murray	Atlanta West
National	3173879	Southeast	South Alabama/Florida Panhandle	DHPSM Angela Jackson	Savannah
National	3173879	Southeast	South Alabama/Florida Panhandle	DHPSM Angela Jackson	Montgomery
National	3173879	Southeast	South Carolina	DHPSM Angela Jackson	Macon
National	3173879	Southeast	South Carolina	DHPSM Angela Jackson	Savannah
National	3173879	Southeast	South Carolina	DHPSM Angela Jackson	Columbia
National	3173879	Southeast	South Florida	DHPSM Kathleen Murray	Fort Lauderdale
National	3173879	Southeast	South Florida	DHPSM Kathleen Murray	Fort Myers
National	3173879	Southeast	South Florida	DHPSM Kathleen Murray	Miami
National	3173879	Western	Arizona	DHPSM Western 01 (Open)	Phoenix North
National	3173879	Western	Arizona	DHPSM Western 01 (Open)	Phoenix South
National	3173879	Western	Hawaii	DHPSM Hawaii 01 (Open)	Hawaii
National	3173879	Western	LA/Nevada	DHPSM Western 01 (Open)	Las Vegas/Fresno
National	3173879	Western	LA/Nevada	DHPSM Franklyn Harris	Los Angeles North
National	3173879	Western	LA/Nevada	DHPSM Franklyn Harris	Los Angeles South
National	3173879	Western	Northern California	DHPSM Western 01 (Open)	Las Vegas/Fresno
National	3173879	Western	Northern California	DHPSM Western 01 (Open)	Reno/Sacramento
National	3173879	Western	Northern California	DHPSM Western 01 (Open)	San Francisco
National	3173879	Western	Pacific Northwest	DHPSM Western 01 (Open)	Portland
National	3173879	Western	Pacific Northwest	DHPSM Western 01 (Open)	Seattle
National	3173879	Western	Southern California	DHPSM Franklyn Harris	San Bernardino
National	3173879	Western	Southern California	DHPSM Franklyn Harris	San Diego North
National	3173879	Western	Southern California	DHPSM Franklyn Harris	San Diego South
