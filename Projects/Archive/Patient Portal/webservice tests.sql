select m.post_date, METRIC_CD, GROUPBY1_CD, GROUPBY1_VALUE, sum(metric_value)
from 
(
        select subject_id, post_date, metric_cd, metric_value
        from metric_data_fact 
        where METRIC_CD in (
                'TREATMENT_ALERT_IND',
                'SCHEDULE_DAYS',
                'Identifier',
                'HIGH_NEG_UF_ALERT_IND'
        )
        and PARENT_ID = '1157'
) m inner join 
(
        select distinct subject_id, post_date
        FROM DIMENSION_DATA_FACT
        where DIMENSION_COMBO_ID in (7)
        and PARENT_ID = '1157'
) df on m.subject_id = df.subject_id and m.post_date = df.post_date
inner join
(
        select distinct subject_id, post_date, DIMENSION_CD GROUPBY1_CD, DIMENSION_VALUE GROUPBY1_VALUE
        FROM DIMENSION_DATA_FACT ddf, DIMENSION_DATA_DIM ddd
        where ddf.DIMENSION_COMBO_ID = ddd.DIMENSION_COMBO_ID
        and ddd.DIMENSION_CD in ('MODALITY')
        and PARENT_ID = '1157'
) d on df.subject_id = d.subject_id and df.post_date = d.post_date
group by m.post_date, METRIC_CD, GROUPBY1_CD, GROUPBY1_VALUE


--/
CREATE OR REPLACE FUNCTION readwebservice(url VARCHAR2)

RETURN CLOB
IS
   pcs UTL_HTTP.html_pieces;
   retv CLOB;
BEGIN
   pcs := UTL_HTTP.request_pieces(url, 5000);
   FOR i IN 1 .. pcs.COUNT
   LOOP
      retv := retv || pcs (i);
   END LOOP;
   RETURN retv;
END;
/


select readwebservice('http://data.montgomerycountymd.gov/resource/viwt-tkfn.json') from dual
select readwebservice('http://www.aviationweather.gov//gis/scripts/MetarJSON.php?priority=5&bbox=-73,40,-70,43') from dual

select utl_http.request('http://data.montgomerycountymd.gov/resource/viwt-tkfn.json') from dual
select utl_http.request('http://www.aviationweather.gov//gis/scripts/MetarJSON.php?priority=5&bbox=-73,40,-70,43') from dual

select utl_http.request('http://10.12.35.92/datasource.php') from dual



SELECT jt.*
from (
    select 
    utl_http.request('http://data.montgomerycountymd.gov/resource/viwt-tkfn.json') as jstring
    from dual
) jdata,
JSON_TABLE(jstring, '$[*]'
COLUMNS (site VARCHAR2(20) PATH '$.facilitynumber',
        space_count NUMBER PATH '$.space_count',
        total_spaces NUMBER PATH '$.total_spaces'
)) AS jt

--&bbox=-73,40,-70,43
SELECT jt.*
from (
    select 
    readwebservice('http://www.aviationweather.gov//gis/scripts/MetarJSON.php?priority=5') as jstring
    from dual
) jdata,
JSON_TABLE(jstring, '$.features[*]'
COLUMNS (
        id VARCHAR2(20) PATH '$.properties.id',
        site VARCHAR2(20) PATH '$.properties.site',
        temp NUMBER PATH '$.properties.temp',
        dewp NUMBER PATH '$.properties.dewp',
        wspd NUMBER PATH '$.properties.wspd',
        wdir NUMBER PATH '$.properties.wdir',
        cover VARCHAR2(20) PATH '$.properties.cover',
        visib NUMBER PATH '$.properties.visib',
        fltcat VARCHAR2(20) PATH '$.properties.fltcat',
        altim NUMBER PATH '$.properties.altim',
        slp NUMBER PATH '$.properties.slp',
        longitude NUMBER PATH '$.geometry.coordinates[0]',
        latitude NUMBER PATH '$.geometry.coordinates[1]'
)) AS jt



SELECT jt.*
from (
    select 
    readwebservice('http://biprotodev01/datasource.php') as jstring
    from dual
) jdata,
JSON_TABLE(jstring, '$.rows[*]'
COLUMNS (
        POST_DATE VARCHAR2(20) PATH '$.POST_DATE',
        METRIC_CD VARCHAR2(20) PATH '$.METRIC_CD',
        GROUPBY1_CD VARCHAR2(20) PATH '$.GROUPBY1_CD',
        GROUPBY1_VALUE VARCHAR2(20) PATH '$.GROUPBY1_VALUE',
        NUMERATOR NUMBER PATH '$.NUMERATOR'
)) AS jt
