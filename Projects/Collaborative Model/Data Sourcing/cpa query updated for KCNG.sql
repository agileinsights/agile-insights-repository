fn_dial.fac_fn_pt_tx_agg_h
fn_dial.fac_mgd_care_adj_tx_h
fn_dial.hosp_rev_agg_h
fn_dial.pc_gl_pt_mod_h
fn_dial.pc_gl_pt_tx_agg_h
fn_dial.pc_gl_tx_bud_h
fn_dial.pc_gl_tx_orgc_h
fn_dial.pc_gl_tx_pln_h
fn_dial.pc_pl_agg_h
 

-- quality metrics

SELECT B.METRIC_DT, CPA.DL_POPULATION_START_DT,
DL.FAC_NAME DL_NM, DL.FAC_ID DL_ID, 
B.METRIC_DIM_ID, D.METRIC_CD METRIC_CD, D.METRIC_DESC METRIC_DESC,
COUNT (
       DISTINCT (CASE
                 WHEN (B.PT_DIM_ID * (CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END)) > 0
                 THEN B.PT_DIM_ID
                 ELSE NULL
                 END))
AS numerator,                                                                                                                                                                                                                                                                 
COUNT(DISTINCT B.PT_DIM_ID) denominator

FROM KCNG.Z_PT_METRIC_FACT B
INNER JOIN KCNG.Z_DIAL_DL_POP_FACT_H CPA ON CPA.DL_DIM_ID = B.DL_DIM_ID
INNER JOIN KCNG.ORG_DIALYSIS_FAC DL ON DL.FAC_DIM_ID = CPA.DL_DIM_ID
INNER JOIN KCNG.Z_INITIATIVE_POP_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
INNER JOIN KCNG.Z_PROG_INIT_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID
INNER JOIN KCNG.Z_METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
INNER JOIN KCNG.Z_QSD_ADMIT_DIM A ON B.DIAL_ADM_DISC_FACT_ID = A.ADMIT_DIM_PK AND A.PT_ACTIVE_1MO_IND = 'Y'

WHERE B.METRIC_DT >= '2015-01-01'
AND (B.DL_DIM_ID > 0)
AND P.PROGRAM_DIM_ID = 7 
AND B.METRIC_DIM_ID IN (
	73, --spkt/v
	1, -- hemo
	131, -- CBM
	228, -- albumin
	85, -- catheters

	238, -- footcheck (needs to be fixed in oracle)
	104, -- fistula without catheter
	27, -- transplant education
	44, -- hospitalization rate
	    -- readmission rate
	72 -- missed treatments
	    -- standard mortality rate  (not in KCNG)
	)
GROUP BY  B.METRIC_DT, CPA.DL_POPULATION_START_DT, DL.FAC_NAME, DL.FAC_ID, B.METRIC_DIM_ID, D.METRIC_CD, D.METRIC_DESC

-- ultrascore subtotal

union
select METRIC_DT,  DL_POPULATION_START_DT,
DL_NM, DL_ID, 888, 'US_SUB' METRIC_CD, 'Ultrascore Subtotal' METRIC_DESC,
	sum(round(cast(numerator as float)/cast(denominator as float), 3)*100) as numerator, 1 as denominator
from (
	SELECT B.METRIC_DT, CPA.DL_POPULATION_START_DT,
	DL.FAC_NAME DL_NM, DL.FAC_ID DL_ID, 
	B.METRIC_DIM_ID, D.METRIC_CD METRIC_CD, D.METRIC_DESC METRIC_DESC,
	COUNT (
	       DISTINCT (CASE
	                 WHEN (B.PT_DIM_ID * (CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END)) > 0
	                 THEN B.PT_DIM_ID
	                 ELSE NULL
	                 END))
	AS numerator,                                                                                                                                                                                                                                                                 
	COUNT(DISTINCT B.PT_DIM_ID) denominator

        FROM KCNG.Z_PT_METRIC_FACT B
        INNER JOIN KCNG.Z_DIAL_DL_POP_FACT_H CPA ON CPA.DL_DIM_ID = B.DL_DIM_ID
        INNER JOIN KCNG.ORG_DIALYSIS_FAC DL ON DL.FAC_DIM_ID = CPA.DL_DIM_ID
        INNER JOIN KCNG.Z_INITIATIVE_POP_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
        INNER JOIN KCNG.Z_PROG_INIT_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID
        INNER JOIN KCNG.Z_METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
        INNER JOIN KCNG.Z_QSD_ADMIT_DIM A ON B.DIAL_ADM_DISC_FACT_ID = A.ADMIT_DIM_PK AND A.PT_ACTIVE_1MO_IND = 'Y'

	WHERE B.METRIC_DT >= '2015-01-01'
	AND (B.DL_DIM_ID > 0)
	AND P.PROGRAM_DIM_ID = 7 
	AND B.METRIC_DIM_ID IN (
		73, --spkt/v
		1, -- hemo
		131, -- CBM
		228, -- albumin
		85 -- catheters
	)
	GROUP BY  B.METRIC_DT, CPA.DL_POPULATION_START_DT, DL.FAC_NAME, DL.FAC_ID, B.METRIC_DIM_ID, D.METRIC_CD, D.METRIC_DESC
) primaryindicators
group by METRIC_DT, DL_POPULATION_START_DT, DL_NM, DL_ID

-- expenses, salaries and ebit
union
SELECT 
TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date,  CPA.DL_POPULATION_START_DT,
DL.FAC_NAME DL_NM, DL.FAC_ID DL_ID, 
999 as METRIC_DIM_ID, translate(FN_CLASS_NAME, ' ', '_') as METRIC_CD, FN_CLASS_NAME as METRIC_DESC,
sum(POST_AMT) AS numerator, max(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
FROM  Z_GL_PC_MTH_AGGR_DIAL GL -- CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H
INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
INNER JOIN KCNG.Z_DIAL_DL_POP_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
INNER JOIN KCNG.ORG_DIALYSIS_FAC DL ON DL.FAC_DIM_ID = CPA.DL_DIM_ID

INNER JOIN KCNG.Z_INITIATIVE_POP_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
INNER JOIN KCNG.Z_PROG_INIT_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID

WHERE P.PROGRAM_DIM_ID = 7 
and FN_CLASS_NAME in ('EBIT', 'Operating Expenses',  'Total Salaries',  'Total Medical Supplies Expenses')
and PL.post_Date_id > '20150101'
group by TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1, CPA.DL_POPULATION_START_DT,
DL.FAC_NAME, DL.FAC_ID, FN_CLASS_NAME
/*
-- medical expenses
union
SELECT 
TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date,  CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID, 
999 as METRIC_DIM_ID, 'tot_med_supply' as METRIC_CD, 'Total Medical Supplies Expenses' as METRIC_DESC,
sum(POST_AMT) AS numerator, sum(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = CPA.DL_DIM_ID

INNER JOIN CS_QUALITY.ADMIN.INITIATIVE_POPULATION_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.PROGRAM_INITIATIVE_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID

WHERE P.PROGRAM_DIM_ID = 7 
and FN_ACCT_NAME = 'Total Medical Supplies Expenses'
and PL.post_Date_id > '20150101'
group by TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1, CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID

-- net revenue
union
SELECT 
TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date,  CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID, 
999 as METRIC_DIM_ID, 'net_revenue' as METRIC_CD, 'Net Revenue' as METRIC_DESC,
sum(POST_AMT) AS numerator, max(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = CPA.DL_DIM_ID

INNER JOIN CS_QUALITY.ADMIN.INITIATIVE_POPULATION_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.PROGRAM_INITIATIVE_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID

WHERE P.PROGRAM_DIM_ID = 7 
and FN_CLASS_NAME in ('Commercial Revenue','Medicaid Risk Revenue','Medicare Revenue','Medicare Advantage Revenue','Medicaid Revenue','Old Revenue',
'Misc Revenue','Self Pay Revenue','Acute Revenue','Vascular Revenue','Bad Debt Revenue','Pre Acute Revenue','CKD Class Revenue','Other Class Revenue','Interest & Taxes')
and PL.post_Date_id > '20150101'
group by TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1, CPA.DL_POPULATION_START_DT, 
DL.DL_NM, DL.DL_ID

-- in center treatments
union
SELECT 
TO_DATE(GL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(GL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date,  CPA.DL_POPULATION_START_DT,
DL.DL_NM, DL.DL_ID, 
999 as METRIC_DIM_ID, 'ic_tmt' as METRIC_CD, 'In Center Treatments' as METRIC_DESC,
sum(GL.ACTL_IC_TMT_CNT) As numerator, 1 AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = GL.DL_DIM_ID

INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON DL.DL_DIM_ID = CPA.DL_DIM_ID

INNER JOIN CS_QUALITY.ADMIN.INITIATIVE_POPULATION_FACT_H I ON CPA.POPULATION_DIM_ID = I.POPULATION_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.PROGRAM_INITIATIVE_FACT_H P ON I.INITIATIVE_DIM_ID = P.INITIATIVE_DIM_ID

WHERE P.PROGRAM_DIM_ID = 7 
and GL.post_Date_id > '20150101'
group by TO_DATE(GL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(GL.POST_DATE_ID, 7, 2) as integer) + 1, CPA.DL_POPULATION_START_DT, 
DL.DL_NM, DL.DL_ID


union

-- All FMC
-- quality metrics

SELECT B.METRIC_DT, TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT,
'All FMC' DL_NM, 999999 DL_ID, 
B.METRIC_DIM_ID, M.METRIC_CD METRIC_CD, M.METRIC_DESC METRIC_DESC,

COUNT (
       DISTINCT (CASE
                 WHEN (B.PT_DIM_ID * (CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END)) > 0
                 THEN B.PT_DIM_ID
                 ELSE NULL
                 END))
AS numerator,                                                                                                                                                                                                                                                                 
COUNT(DISTINCT B.PT_DIM_ID) denominator

FROM CS_QUALITY.ADMIN.PATIENT_METRIC_FACT B
-- INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = B.DL_DIM_ID
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = B.DL_DIM_ID

INNER JOIN CS_QUALITY.ADMIN.METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
INNER JOIN CS_QUALITY.ADMIN.METRIC_DIM M ON D.METRIC_DIM_ID = M.METRIC_DIM_ID

INNER JOIN CS_QUALITY.ADMIN.QSD_ADMIT_DIM A ON B.DIAL_ADM_DISC_FACT_ID = A.ADMIT_DIM_PK AND A.PT_ACTIVE_1MO_IND = 'Y'

WHERE B.METRIC_DT >= '2015-01-01'
AND (B.DL_DIM_ID > 0)
AND B.METRIC_DIM_ID IN (
	73, --spkt/v
	1, -- hemo
	131, -- CBM
	228, -- albumin
	85, -- catheters

	238, -- footcheck (needs to be fixed in oracle)
	104, -- fistula without catheter
	27, -- transplant education
	44, -- hospitalization rate
	    -- readmission rate
	72 -- missed treatments
	    -- standard mortality rate  (not in KCNG)
	)
GROUP BY  B.METRIC_DT, B.METRIC_DIM_ID, M.METRIC_CD, M.METRIC_DESC

-- ultrascore subtotal

union
select METRIC_DT,  DL_POPULATION_START_DT,
DL_NM, DL_ID, 888, 'US_SUB' METRIC_CD, 'Ultrascore Subtotal' METRIC_DESC,
	sum(round(cast(numerator as float)/cast(denominator as float), 3)*100) as numerator, 1 as denominator
from (
	SELECT B.METRIC_DT, TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT, 
	'All FMC' DL_NM, 999999 DL_ID, 
	B.METRIC_DIM_ID, M.METRIC_CD METRIC_CD, M.METRIC_DESC METRIC_DESC,
	COUNT (
	       DISTINCT (CASE
	                 WHEN (B.PT_DIM_ID * (CASE WHEN B.IS_PT_METRIC_TRUE_IND THEN 1 ELSE 0 END)) > 0
	                 THEN B.PT_DIM_ID
	                 ELSE NULL
	                 END))
	AS numerator,                                                                                                                                                                                                                                                                 
	COUNT(DISTINCT B.PT_DIM_ID) denominator

	FROM CS_QUALITY.ADMIN.PATIENT_METRIC_FACT B
--	INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = B.DL_DIM_ID
	INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = B.DL_DIM_ID

	INNER JOIN CS_QUALITY.ADMIN.METRIC_TARGET_FACT D ON B.METRIC_DIM_ID = D.METRIC_DIM_ID
	INNER JOIN CS_QUALITY.ADMIN.METRIC_DIM M ON D.METRIC_DIM_ID = M.METRIC_DIM_ID

        INNER JOIN CS_QUALITY.ADMIN.QSD_ADMIT_DIM A ON B.DIAL_ADM_DISC_FACT_ID = A.ADMIT_DIM_PK AND A.PT_ACTIVE_1MO_IND = 'Y'

	WHERE B.METRIC_DT >= '2015-01-01'
	AND (B.DL_DIM_ID > 0)
	AND B.METRIC_DIM_ID IN (
		73, --spkt/v
		1, -- hemo
		131, -- CBM
		228, -- albumin
		85 -- catheters
	)
	GROUP BY  B.METRIC_DT, B.METRIC_DIM_ID, M.METRIC_CD, M.METRIC_DESC
) primaryindicators
group by METRIC_DT,  DL_POPULATION_START_DT, DL_NM, DL_ID


-- expenses, salaries and ebit
union
select metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC, sum(numerator) numerator, sum(denominator) as denominator
from (
    SELECT PL.DL_DIM_ID, 
    TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date, 
    TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT,
    'All FMC' DL_NM, 999999 DL_ID, 
    999 as METRIC_DIM_ID, translate(FN_CLASS_NAME, ' ', '_') as METRIC_CD, FN_CLASS_NAME as METRIC_DESC,
    sum(POST_AMT) AS numerator, max(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
    FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
    INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
    INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
    -- INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
    INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = PL.DL_DIM_ID

    WHERE FN_CLASS_NAME in ('EBIT', 'Operating Expenses',  'Total Salaries',  'Total Medical Supplies Expenses')
    and PL.post_Date_id > '20150101'
    group by PL.DL_DIM_ID, TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1, 
    FN_CLASS_NAME
) dldetail
group by metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC

-- medical expenses
union
select metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC, sum(numerator) numerator, sum(denominator) as denominator
from (
    SELECT PL.DL_DIM_ID, 
    TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date,  
    TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT,
    'All FMC' DL_NM, 999999 DL_ID, 
    999 as METRIC_DIM_ID, 'tot_med_supply' as METRIC_CD, 'Total Medical Supplies Expenses' as METRIC_DESC,
    sum(POST_AMT) AS numerator, max(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
    FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
    INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
    INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
    -- INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
    INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = PL.DL_DIM_ID

    WHERE FN_ACCT_NAME = 'Total Medical Supplies Expenses'
    and PL.post_Date_id > '20150101'
    group by PL.DL_DIM_ID, TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1
) dldetail
group by metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC

-- net revenue
union
select metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC, sum(numerator) numerator, sum(denominator) as denominator
from (
    SELECT PL.DL_DIM_ID, 
    TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date,  
    TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT,
    'All FMC' DL_NM, 999999 DL_ID, 
    999 as METRIC_DIM_ID, 'net_revenue' as METRIC_CD, 'Net Revenue' as METRIC_DESC,
    sum(POST_AMT) AS numerator, max(GL.ACTL_DIAL_MOD_TMT_CNT) AS denominator
    FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
    INNER JOIN CS_REVENUE_NEW.ADMIN.PC_PL_AGGREGATE_FACT_H PL ON GL.DL_DIM_ID = PL.DL_DIM_ID and GL.POST_DATE_ID = PL.POST_DATE_ID
    INNER JOIN CS_REVENUE_NEW.ADMIN.FINANCE_ACCOUNT_DIM ACC ON PL.FN_ACCOUNT_DIM_ID = ACC.FN_ACCOUNT_DIM_ID
    -- INNER JOIN CS_QUALITY.ADMIN.DIALYSIS_DL_POPULATION_FACT_H CPA ON CPA.DL_DIM_ID = PL.DL_DIM_ID
    INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = PL.DL_DIM_ID

    WHERE FN_CLASS_NAME in ('Commercial Revenue','Medicaid Risk Revenue','Medicare Revenue','Medicare Advantage Revenue','Medicaid Revenue','Old Revenue',
    'Misc Revenue','Self Pay Revenue','Acute Revenue','Vascular Revenue','Bad Debt Revenue','Pre Acute Revenue','CKD Class Revenue','Other Class Revenue','Interest & Taxes')
    and PL.post_Date_id > '20150101'
    group by PL.DL_DIM_ID, TO_DATE(PL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(PL.POST_DATE_ID, 7, 2) as integer) + 1
) dldetail
group by metric_date, DL_POPULATION_START_DT, DL_NM, DL_ID, METRIC_DIM_ID, METRIC_CD, METRIC_DESC

-- in center treatments
union
SELECT 
TO_DATE(GL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(GL.POST_DATE_ID, 7, 2) as integer) + 1 as metric_date,  
TO_DATE('20150101', 'YYYYMMDD') DL_POPULATION_START_DT,
'All FMC' DL_NM, 999999 DL_ID, 
999 as METRIC_DIM_ID, 'ic_tmt' as METRIC_CD, 'In Center Treatments' as METRIC_DESC,
sum(GL.ACTL_IC_TMT_CNT) As numerator, 1 AS denominator
FROM CS_REVENUE_NEW.ADMIN.PC_GL_PT_TX_AGGREGATE_FACT_H GL
INNER JOIN CONFORMED_DIMS.ADMIN.DIALYSIS_LOCATION_DIM DL ON DL.DL_DIM_ID = GL.DL_DIM_ID

WHERE GL.post_Date_id > '20150101'
group by TO_DATE(GL.POST_DATE_ID, 'YYYYMMDD') - cast(substr(GL.POST_DATE_ID, 7, 2) as integer) + 1

*/