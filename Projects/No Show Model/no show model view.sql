

create or replace view Z_NSM_OUTPUT_FACT as
(
SELECT 
source_nm,
DIAL_POP_DATA_FACT.subject_id as MRN,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.Obs') as Obs,
NVL(json_value(DIAL_POP_DATA_FACT.subject_json, '$.clinic'), 
        json_value(DIAL_POP_DATA_FACT.subject_json, '$.FHPClinicNum')) as clinic,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.PTLNAME') as PTLNAME,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.PTFNAME') as PTFNAME,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.incident') as incident,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.prob1') as prob1,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.prob2') as prob2,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.prob3') as prob3,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.prob4') as prob4,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.prob5') as prob5,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.avg_change') as avg_change,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.curr_unoshow') as curr_unoshow,
json_value(reason_codes1.subject_json, '$.desc') as reason_desc1,
json_value(reason_codes2.subject_json, '$.desc') as reason_desc2,
json_value(reason_codes3.subject_json, '$.desc') as reason_desc3,
json_value(reason_codes4.subject_json, '$.desc') as reason_desc4,
json_value(reason_codes5.subject_json, '$.desc') as reason_desc5,
  "DIAL_POP_DATA_FACT"."INSERT_DATE" AS "INSERT_DATE",
  "DIAL_POP_DATA_FACT"."INSERT_BY" AS "INSERT_BY"

FROM "BI_SRC"."DIAL_POP_DATA_FACT" "DIAL_POP_DATA_FACT"
        inner join DIAL_POP_DATA_SOURCE_DIM 
        on DIAL_POP_DATA_FACT.SOURCE_ID = DIAL_POP_DATA_SOURCE_DIM.SOURCE_ID
        left join "BI_SRC"."DIAL_POP_DATA_FACT" reason_codes1
        on json_value(DIAL_POP_DATA_FACT.subject_json, '$.top1_riskfactor') = reason_codes1.subject_id and reason_codes1.source_id = 4
        left join "BI_SRC"."DIAL_POP_DATA_FACT" reason_codes2
        on json_value(DIAL_POP_DATA_FACT.subject_json, '$.top2_riskfactor') = reason_codes2.subject_id and reason_codes2.source_id = 4
        left join "BI_SRC"."DIAL_POP_DATA_FACT" reason_codes3
        on json_value(DIAL_POP_DATA_FACT.subject_json, '$.top3_riskfactor') = reason_codes3.subject_id and reason_codes3.source_id = 4
        left join "BI_SRC"."DIAL_POP_DATA_FACT" reason_codes4
        on json_value(DIAL_POP_DATA_FACT.subject_json, '$.top4_riskfactor') = reason_codes4.subject_id and reason_codes4.source_id = 4
        left join "BI_SRC"."DIAL_POP_DATA_FACT" reason_codes5
        on json_value(DIAL_POP_DATA_FACT.subject_json, '$.top5_riskfactor') = reason_codes5.subject_id and reason_codes5.source_id = 4
where DIAL_POP_DATA_FACT.source_id in (2,3)
--and "DIAL_POP_DATA_FACT"."INSERT_DATE" > (select max("DIAL_POP_DATA_FACT"."INSERT_DATE") from "DIAL_POP_DATA_FACT" where DIAL_POP_DATA_FACT.source_id in (2,3)) - 1
)


create or replace view Z_NSM_OUTPUT_FACT as
(
SELECT 
json_value(subject_json, '$.mrn') as MRN,
json_value(subject_json, '$.PTLNAME') as PTLNAME,
json_value(subject_json, '$.PTFNAME') as PTFNAME,
json_value(subject_json, '$.HOME_PHONE') as PHONE,
json_value(subject_json, '$.FHPClinicNum') || '-' || json_value(subject_json, '$.FAC_NM') as clinic,
json_value(subject_json, '$.customer') as customer,
json_value(subject_json, '$.avg_change') as avg_change,

to_date(json_value(subject_json, '$.metric_dt'), 'YYYY-MM-DD HH24:MI:SS') as METRIC_DT,
json_value(subject_json, '$.METRIC_DESC') as METRIC_DESC,
json_value(subject_json, '$.METRIC_VALUE') as METRIC_VALUE,
json_value(subject_json, '$.attr1') as attr1,
to_char(json_value(subject_json, '$.attr2')) as attr2

FROM (
        select subject_json
        FROM dial_pop_data_fact
        where source_id = 6
        and insert_date > (select max(insert_date) from dial_pop_data_fact where source_id = 6) - 1
       ) last_load

 union
 SELECT 
 "DIAL_POP_DATA_FACT"."SUBJECT_ID" AS "SUBJECT_ID",
 null as PTLNAME,
 null as PTFNAME,
 null as PHONE,
 null as clinic,
 null as customer,
 null as avg_change,
  "DIAL_POP_DATA_FACT"."INSERT_DATE" AS METRIC_DT,
  'Call Made' as metric_desc,
  '1' as METRIC_VALUE,
  'Call Made' as attr1,
  to_char(json_value(DIAL_POP_DATA_FACT.subject_json, '$.call_made')) || ' by ' || to_char("DIAL_POP_DATA_FACT"."INSERT_BY") AS "attr2"
FROM "BI_SRC"."DIAL_POP_DATA_FACT" "DIAL_POP_DATA_FACT"
where source_id = 1
)

 
create or replace view Z_NSM_OUTPUT_FACT1 as
(
SELECT 
json_value(DIAL_POP_DATA_FACT.subject_json, '$.mrn') as mrn,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.type') as type,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.customer') as customer,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.Obs') as Obs,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.id') as id,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.FHPClinicNum') as FHPClinicNum,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.FAC_NM') as FAC_NM,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.PTLNAME') as PTLNAME,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.PTFNAME') as PTFNAME,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.incident') as incident,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.prob1') as prob1,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.prob2') as prob2,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.prob3') as prob3,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.prob4') as prob4,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.prob5') as prob5,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.avg_change') as avg_change,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.curr_unoshow') as curr_unoshow,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.top1_riskfactor') as top1_riskfactor,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.top2_riskfactor') as top2_riskfactor,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.top3_riskfactor') as top3_riskfactor,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.top4_riskfactor') as top4_riskfactor,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.top5_riskfactor') as top5_riskfactor,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.PT_DIM_ID') as PT_DIM_ID,
to_date(json_value(DIAL_POP_DATA_FACT.subject_json, '$.METRIC_DT'), 'YYYY-MM-DD HH24:MI:SS') as METRIC_DT,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.PT_FRMTTD_NM') as PT_FRMTTD_NM,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.RACE_NM') as RACE_NM,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.MARITAL_STAT_NM') as MARITAL_STAT_NM,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.BIRTH_DT') as BIRTH_DT,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.GENDER_NM') as GENDER_NM,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.PREF_LANG_NM') as PREF_LANG_NM,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.METRIC_DESC') as METRIC_DESC,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.METRIC_VALUE_TXT') as METRIC_VALUE_TXT,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.METRIC_DIM_ID') as METRIC_DIM_ID,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.PATIENT_ID') as PATIENT_ID,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.HOME_PHONE') as HOME_PHONE,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.FirstReason') as FirstReason,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.SecondReason') as SecondReason,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.ThirdReason') as ThirdReason,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.FourthReason') as FourthReason,
json_value(DIAL_POP_DATA_FACT.subject_json, '$.FifthReason') as FifthReason

FROM (
        select subject_json
        FROM dial_pop_data_fact
        where source_id = 7
        and insert_date > (select max(insert_date) from dial_pop_data_fact where source_id = 7) - 1
       ) DIAL_POP_DATA_FACT
 )